#!/bin/env python
"""
Compute cyclone budget for the Antarctic
This means counting cyclones in boxes:
    - total numbers of positions
    - total numbers of systems
    - genesis numbers
    - lysis numbers
    - in/out from the residing boxes
- All counts per month
- and per boxes of 4x4deg
- output with counts per year-month-domain
"""

__Author__="<petteri.uotila@csiro.au>"
__Date__="27/04/2012"

import os,sys,shutil,subprocess
from merge_sqlite_dbs import fetchCyclones
from datetime import datetime,timedelta
from pyproj import Geod
import numpy
import cPickle, gzip

class Domain:
    def __init__(self,number,year,month,latmin,latmax,lonmin,lonmax):
        self.number = number # ALL,IOS,...
        self.year = year
        self.month = month
        self.lat = 0.5*(latmax+latmin)
        self.lon = (0.5*(lonmax+lonmin))%360
        self.latrange = [latmin,latmax]
        self.lonrange = [lonmin%360,lonmax%360]
        dlon = self.lonrange[1]-self.lonrange[0]
        # area of domain in dlat**2
        self.area = (self.latrange[1]-self.latrange[0])*\
                    numpy.cos(self.lat*numpy.pi/180.)*\
                    (self.lonrange[1]-self.lonrange[0])
        if self.area<0:
            self.area = (self.latrange[1]-self.latrange[0])*\
                        numpy.cos(self.lat*numpy.pi/180.)*\
                        (360-self.lonrange[0]+self.lonrange[1])
        self.daterange = self.get_daterange(year,month)
        self.counts = {'total':0,'system':0,\
                'genesis':0,'lysis':0,\
                'outN':0,'inN':0,\
                'outS':0,'inS':0,\
                'outE':0,'inE':0,\
                'outW':0,'inW':0}

    def get_daterange(self,year,month):
        mindate = datetime(year,month,1)
        if month<12:
           maxdate = datetime(year,month+1,1)-timedelta(days=1)
        else:
           maxdate = datetime(year+1,1,1)-timedelta(days=1)
        return [mindate,maxdate]

    def isInside(self,lat,lon,year=None,month=None):
        """ Returns True if lat,lon inside domain
        """
        lon %= 360
        if lat>=self.latrange[0] and \
           lat<self.latrange[1]  and \
           (year is None or year==self.year) and \
           (month is None or month==self.month):
               if lon>=self.lonrange[0] and \
                  lon<self.lonrange[1] and \
                  self.lonrange[1]>self.lonrange[0]:
                   return True
               # the domain goes across the zero meridian
               elif (lon<self.lonrange[1] or \
                     lon>=self.lonrange[0]) and \
                     self.lonrange[1]<self.lonrange[0]:
                   return True
               else:
                   return False
        else:
               return False

    def update_counts(self,cyclone,year,month):
        g = Geod(ellps='WGS84')
        # genesis count
        if self.isInside(cyclone.gen_lat,cyclone.gen_lon,cyclone.genesis.year,cyclone.genesis.month):
           self.counts['genesis'] += 1
        # lysis count
        if self.isInside(cyclone.lys_lat,cyclone.lys_lon,cyclone.lysis.year,cyclone.lysis.month):
           self.counts['lysis'] += 1
        ZeroCount = True
        for lidx,location in enumerate(cyclone.locations):
            if self.isInside(location.y,location.x,location.year,location.month):
               # total count
               self.counts['total'] += 1
               # individual system count
               if ZeroCount:
                  self.counts['system'] += 1
                  ZeroCount = False
            else:
                # current location is not in the domain
                # FirstCount = False
                continue
            # fluxes in
            if lidx>0:
                prevloc = cyclone.locations[lidx-1]
                if not self.isInside(prevloc.y,prevloc.x,prevloc.year,prevloc.month):
                    az12,az21,dist = g.inv(prevloc.x,prevloc.y,self.lon,self.lat)
                    az21 %= 360
                    #print "I: (%f,%f)->(%f,%f) %f" % (prevloc.x,prevloc.y,self.lon,self.lat,az21)
                    # use back azimuth az21 to define direction where cyclone came
                    # sectors are: N -45 to 45, E 45 to 135, S 135 to -135, W
                    # -135 to -45. There is a singularity: if back and forward azimuths are 0 then inN
                    # if back azimuth is zero but forward -180 then inS
                    if az21==0.0 and az12==0.0:
                        self.counts['inN'] += 1
                    elif az21==0.0 and az12==180.0:
                        self.counts['inS'] += 1
                    else:
                        if az21>315.0 or az21<=45.:
                            self.counts['inN'] += 1
                        if az21>45.0 and az21<=135.:
                            self.counts['inE'] += 1
                        if az21>135.0 and az21<=225.:
                            self.counts['inS'] += 1
                        if az21>225.0 and az21<=315.:
                            self.counts['inW'] += 1
            # fluxes out
            if lidx<len(cyclone.locations)-1:
                nextloc = cyclone.locations[lidx+1]
                if not self.isInside(nextloc.y,nextloc.x,nextloc.year,nextloc.month):
                    az12,az21,dist = g.inv(self.lon,self.lat,nextloc.x,nextloc.y)
                    az12 %= 360
                    #print "O: (%f,%f)->(%f,%f) %f" % (self.lon,self.lat,nextloc.x,nextloc.y,az12)
                    # use forward azimuth az12 to define direction where cyclone went
                    if az21==0.0 and az12==0.0:
                        self.counts['outN'] += 1
                    elif az21==0.0 and az12==180.0:
                        self.counts['outS'] += 1
                    else:
                        if az12>315.0 or az12<=45.:
                            self.counts['outN'] += 1
                        if az12>45.0 and az12<=135.:
                            self.counts['outE'] += 1
                        if az12>135.0 and az12<=225.:
                            self.counts['outS'] += 1
                        if az12>225.0 and az12<=315.:
                            self.counts['outW'] += 1
class Domains:
    """
    A collection of domains
    """
    def __init__(self,years,dlon=20.,dlat=4.):
        lon1d = numpy.arange(0,360,dlon)
        lat1d = numpy.arange(-86,-26,dlat)
        self.lon2d, self.lat2d = numpy.meshgrid(lon1d,lat1d)
        # lat and lon bounds
        lonb1d = numpy.concatenate((lon1d-0.5*dlon,[lon1d[-1]+0.5*dlon]))
        latb1d = numpy.concatenate((lat1d-0.5*dlat,[lat1d[-1]+0.5*dlat]))
        self.lonb2d, self.latb2d = numpy.meshgrid(lonb1d,latb1d)
        # init table structure as [lat][lon][year][month]
        self.domains = [[[[[]for m in range(12)] for y in range(len(years))] \
                             for n in range(len(lon1d))] for m in range(len(lat1d))]
        domainno = 1
        for lati,lat in enumerate(lat1d):
            for loni,lon in enumerate(lon1d):
                for yri,year in enumerate(years):
                    for month in range(1,13):
                        self.domains[lati][loni][yri][month-1] = \
                             Domain(domainno,year,month,\
                             lat-0.5*dlat,lat+0.5*dlat,\
                             lon-0.5*dlon,lon+0.5*dlon)
                domainno += 1
        self.areas = numpy.reshape([self.domains[i][j][0][0].area \
                                    for i in range(len(self.domains)) \
                                    for j in range(len(self.domains[0]))],\
                                    (len(self.domains),len(self.domains[0])))
        self.lat1d = lat1d
        self.lon1d = lon1d
        self.years = years

    def update_counts(self,cyclone):
        for lati,lat in enumerate(self.lat1d):
            for loni,lon in enumerate(self.lon1d):
                for yri,year in enumerate(self.years):
                    for month in range(1,13):
                        domain = self.domains[lati][loni][yri][month-1]
                        domain.update_counts(cyclone,year,month)
        return len(cyclone.locations)

    def get_values(self,name='system',yidx=0,midx=0):
        """
        Returns counts of name as a matrix
        yidx = year index, midx = month index 0..11
        """
        v1d = [self.domains[lati][loni][yidx][midx].counts[name] \
               for lati,lat in enumerate(self.lat1d) \
               for loni,lon in enumerate(self.lon1d)]
        v2d = numpy.reshape(v1d,self.lat2d.shape)
        return numpy.array(v2d)

if __name__=="__main__":
        syear      = int(sys.argv[1])
        eyear      = int(sys.argv[2])
        years      = range(syear,eyear+1)
        scratchdir = './'
        model      = 'cfsr'
        bname      = 'muni_erai_cyclone_budget_box_aa_%04d-%04d' % \
                      (years[0],years[-1])
        fname      = "%s.png" % bname
        dname      = "%s.domains.cpickle.gz" % bname
        cname      = "%s.cpickle.gz" % bname
        months     = [0] # all months
        # init domains
        domains = Domains(years)
        ###
        nlocs = 0; ncycs = 0
        # select systems that live longer or equal to 24h and
        # are generated south of or equal to 40S and
        # do not cross terrain higher than 500m or
        # travel longer than 1000km
        criteria = \
        "((MUNICFSRCyclonesWithPOD.Cyclone.where.lifetime_in_secs>=86400.) & \
          (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat<=-40.) & \
          ((MUNICFSRCyclonesWithPOD.Cyclone.where.max_terrain<=500.) | \
           (MUNICFSRCyclonesWithPOD.Cyclone.where.beeline>=1000.)))"
        cyclones = fetchCyclones(years=years,criteria=criteria,months=months)
        #cyclones = fetchCyclones(years=years,criteria=criteria,months=range(1,2))
        print "Fetched cyclones."
        sys.stdout.flush()
        for cyclone in cyclones:
            ncycs += 1
            nlocs += domains.update_counts(cyclone)
        print "Updated cyclone counts."
        # store domains
        fp = gzip.open(dname,'w')
        cPickle.dump(domains,fp)
        fp.close()
        #sys.exit(0)
        sys.stdout.flush()
        names = ['total','system','genesis','lysis',\
                 'outE','inW','outW','inE',\
                 'outS','inN','outN','inS']
        values = {}
        for name in names:
            values[name] = numpy.zeros(domains.lat2d.shape)
            for yidx,year in enumerate(domains.years):
                for midx in range(12):
                    values[name] += domains.get_values(name=name,midx=midx,yidx=yidx)
                print "Processed year %04d." % year
                sys.stdout.flush()
            print "Processed variable %s." % name
            sys.stdout.flush()
        # store values
        fp = gzip.open(cname,'w')
        cPickle.dump(values,fp)
        fp.close()
        # plot values
        import matplotlib.pyplot as plt
        from mpl_toolkits import basemap
        m = basemap.Basemap(projection='spstere',boundinglat=-30.,lon_0=0.)
        x, y = m(domains.lonb2d,domains.latb2d)
        fig = plt.figure(figsize=(14,9))
        for fidx,name in enumerate(names):
            ax = fig.add_subplot(3,4,fidx+1)
            img = m.pcolor(x,y,values[name]/domains.areas,cmap='binary',edgecolors='k')
            fig.colorbar(img)
            ax.set_title("sum(%s)=%d" % \
                         (name,numpy.sum(values[name])))
            m.drawcoastlines()
        plt.savefig(fname)
        print "ncycs=%d, nlocs=%d, Finished!" % (ncycs,nlocs)
###
