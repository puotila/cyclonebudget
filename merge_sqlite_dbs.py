#!/bin/env python
"""
Merges sqlite dbs created with pod
- check if systems cross domain boundaries
"""

import sys,os,numpy
sys.path.append(os.path.join(os.environ['HOME'],'lib','python2.7','site-packages','pod-0.54.2-py2.7.egg'))

class CycloneLocs:
    """
    Just a simple class to store information from db cyclones
    """
    def __init__(self):
        self.lon = []
        self.lat = []

def getAllCriteria():
    return "(MUNICFSRCyclonesWithPOD.Cyclone.where.beeline>=0.)"

def getGeneralCriteria():
    """ This query gets all sensible systems from data
    lifetime >= 12h, over terrain below 500m
    """
    return "((MUNICFSRCyclonesWithPOD.Cyclone.where.lifetime_in_secs>=43200.) & \
    ((MUNICFSRCyclonesWithPOD.Cyclone.where.max_terrain<=500.) | \
    (MUNICFSRCyclonesWithPOD.Cyclone.where.beeline>=1000.)))"
# when max_terrain is a limit systems transporting heat and moisture deep into
# aa from ocean are excluded

#def getGeneralCriteria():
#    """ This query gets all sensible systems from data
#    lifetime >= 12h, born and die inside domain
#    """
#    return "((MUNICFSRCyclonesWithPOD.Cyclone.where.lifetime_in_secs>=43200.) & \
#    (MUNICFSRCyclonesWithPOD.Cyclone.where.boundary==0))"

def getBombsCriteria():
    """ This query gets all bomb systems from datae
    where a function dp/dt>24hPa/24hrs was applied (bomb==1).
    """
    criteria = getGeneralCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.bomb==1))"

def getSmallBombsCriteria():
    """ This query gets all bomb systems from data
    where a function dp/dt>24hPa/24hrs was applied (bomb==1).
    """
    criteria = getBombsCriteria()
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.max_rd<=4.5))"

def getSmallOnesCriteria():
    """ This query gets small systems with diameter<1000 km
    beeline longer than 750 km causes suspicious systems almost disappear
    but small systems over the Bellingshausen Sea remain. max_terrain should now
    fix it.
    """
    criteria = getGeneralCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.max_rd<=4.5))"

def getLargeOnesCriteria():
    """ This query gets large systems with diameter>1000 km
    """
    criteria = getGeneralCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.max_rd>4.5))"

def getNHCriteria():
    """ This query gets all NH systems
    """
    criteria = getGeneralCriteria()
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat>0.))"

def getSHCriteria():
    """ This query gets all SH systems
    """
    criteria = getGeneralCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat<0.))"

def getGeneralSo55SCriteria():
    """ This query gets all systems south of 55S
    """
    criteria = getGeneralCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat<-55.))"

def getGeneralSo55SOverIceCriteria():
    """ This query gets all systems south of 55S and over sea-ice part of their life
    """
    criteria = getGeneralSo55SCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.max_cice>0.))"

def getLargeOnesSo55SCriteria():
    """ This query gets large systems south of 55S
    """
    criteria = getLargeOnesCriteria()    
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat<-55.))"

def getSmallOnesSo55SCriteria():
    """ This query gets small systems south of 55S
    """
    criteria = getSmallOnesCriteria()
    return criteria[:-1]+" & (MUNICFSRCyclonesWithPOD.Cyclone.where.gen_lat<-55.))"

def func1stDayMean(list,dt=3):
    """
    Assumes dt=3h and no missing values
    """
    return numpy.mean(list[:24/dt])

def isBomb(cyc,dt=3,thold=24.):
    """
    Returns True if pressure tendency >24hPa/24hrs i.e. is a bomb
    """
    if cyc.lifetime_in_secs<86400.: return
    #vals = []
    #for loc in cyc.locations:
    #    vals.append(loc.p)
    vals = [loc.p for loc in cyc.locations]
    if len(vals)<24/dt: return False
    for i in range(len(vals)-1):
        for j in range(i+1,len(vals)):
            dv = vals[i]-vals[j]
            if dv>=24. and j-i<=24/dt:
                return True
    return False

def isDJF(cyc):
    if [12,1,2].count(cyc.genesis.month):
        return True
    return False

def isMAM(cyc):
    if [3,4,5].count(cyc.genesis.month):
        return True
    return False

def isJJA(cyc):
    if [6,7,8].count(cyc.genesis.month):
        return True
    return False

def isSON(cyc):
    if [9,10,11].count(cyc.genesis.month):
        return True
    return False

def funcMaxTendency(list,dt=3):
    """
    Max change of a system variable in time 
    """
    max_abs_change = numpy.max(numpy.abs(numpy.diff(list)))
    max_abs_change_index = numpy.where(numpy.abs(numpy.diff(list))==max_abs_change)
    return numpy.diff(list)[max_abs_change_index][0]

def fetchCycsFromDb(dbfile,critstr=None,cycfunc=None):
    # clear out the earlier modules [we can open multiple sqlite files for MUNICFSRCyclonesWithPOD]
    for key in sys.modules.keys():
        if key.startswith("MUNI") or key.startswith("pod"):
            sys.modules.pop(key)
    # and reload
    import pod, pod.list, re
    import MUNICFSRCyclonesWithPOD
    year = int(re.search("(\d{4})",dbfile).groups()[0]) # need to find 4digit year from filename
    db = pod.Db(file=dbfile,dynamic_index=True)
    if critstr is None:
        critstr = getGeneralCriteria()
    if cycfunc is None: cycfunc=lambda x: True
    print "dbfile: %s" % dbfile
    print critstr
    acycs = eval(critstr)
    return [cyc for cyc in acycs if cyc.genesis.year==year and cycfunc(cyc)]

def getCycLocAttrIdx(cyc,attrname='max_c',locattrname='c'):
    attridx = None
    if hasattr(cyc,attrname):
        cycattr = getattr(cyc,attrname); idx = 0
        for location in cyc.locations:
            if getattr(location,locattrname)==cycattr:
                attridx = idx
            idx += 1
    return attridx

def fetchCycAttrFromDb(dbfile,attrname,critstr=None,locattrname=None,\
                           attrfunc=None,cycfunc=None,sumattrname='max_c'):
    """ system has to return true to cycfunc
    sumattr is attribute that locattr is summed until sumattr occurs.
    For example locattr is summed until sumattr=max_c
    """
    if attrfunc is None: attrfunc = lambda x: x
    cycs = fetchCycsFromDb(dbfile,critstr,cycfunc)
    attrlist = []; ncycs = 0
    for cyc in cycs:
        if hasattr(cyc,attrname):
            attr = getattr(cyc,attrname)
            if attrname=='locations':
                locattrlist = [getattr(loc,locattrname) for loc in attr if hasattr(loc,locattrname)]
                attrlist.extend(attrfunc(locattrlist))
            else:
                attrlist.append(attr)
            ncycs += 1
    print "N(cycs): %d" % ncycs
    print "N(attrs): %d" % len(attrlist)
    return attrlist

def _getDbFile(year,month=0,computer='nomet',path=None):
    """ Change to nomet below if not meteorology files required
    """
    import socket,re
    if month:
        if computer is None:
            dbfile = "MuniCFSRCyclones_all_exp001_%04d%02d.sqlite" % (year,month)
        else:
            dbfile = "MuniCFSRCyclones_all_exp001_%04d%02d.%s.sqlite" % (year,month,computer)
    else:
        if computer is None:
            dbfile = "MuniCFSRCyclones_all_exp001_%04d-%04d.sqlite" % (year,year)
        else:
            dbfile = "MuniCFSRCyclones_all_exp001_%04d-%04d.%s.sqlite" % (year,year,computer)
    return dbfile

def getScratchPath():
    if os.path.isdir('/scratch'):
        sdir = os.path.join('/scratch',os.environ['LOGNAME'])
        if not os.path.isdir(sdir): os.mkdir(sdir)
        return sdir
    else:
        return '.'

def copyDbFileToScratch(dbfile):
    """
    if scratch exists copies dbfile to it for fast access
    """
    if not os.path.isfile(dbfile):
        print "Cant find %s!" % dbfile; return None
    # on msg see if /scratch exists
    if os.path.isdir('/scratch'):
        sdir = getScratchPath()
        sdbfile = os.path.join(sdir,dbfile)
        if not os.path.isfile(sdbfile) or \
               int(os.path.getmtime(dbfile))>int(os.path.getmtime(sdbfile)) or \
               int(os.path.getsize(dbfile))>int(os.path.getsize(sdbfile)):
            import shutil
            shutil.copy2(dbfile,sdbfile)
            print "Copied %s to %s" % (dbfile,sdir); sys.stdout.flush()
        os.chmod(sdbfile,0600) # r,w access
        return sdbfile # quick access disk
    else:
        return dbfile # current work dir

def fetchAttrLst(attrname,locattrname=None,attrfunc=None,cycfunc=None,criteria=None,years=range(2001,2010),\
                     months=range(1,13),colidx=0,computer='nomet'):
    """
    if you want to read annual files give months=[0]
    areal statistics under a cyclone, colidx=mean:0,std:1,max:2,min:3
    """
    attrlist = []
    for year in years:
        for month in months:
            dbfile   = _getDbFile(year,month,computer=computer)
            if dbfile is None: continue
            attrlist.extend(fetchCycAttrFromDb(dbfile,attrname,criteria,locattrname,attrfunc,cycfunc))
    try:
        return numpy.array(attrlist)[:,colidx]
    except:
        return numpy.array(attrlist)

def getAttr(attrname,years,season,size,colidx=0):
    # based on sea-ice extent
    if season=='winter':
        months = range(6,12)
    elif season=='summer':
        months = [12,1,2,3,4,5]
    elif season=='jjas': # coldest winter
        months = [6,7,8,9]
    else: # all
        months = range(1,13)
    if size=='large':
        criteria = getLargeOnesCriteria()
    else:
        criteria = getSmallOnesCriteria()
    attr = numpy.array(fetchAttrLst(attrname,criteria=criteria,\
                                        years=years,months=months,\
                                        colidx=colidx))
    if attrname[:4]=='sum_':
        nlocs = numpy.array(fetchAttrLst('nlocs',criteria=criteria,\
                                         years=years,months=months))
        attr /= nlocs # normalise sum to time avg
    return attr

def getLocAttr(locattrname,years,season,size,colidx=0):
    # based on sea-ice extent
    if season=='winter':
        months = range(6,12)
    elif season=='summer':
        months = [12,1,2,3,4,5]
    elif season=='jjas': # coldest winter
        months = [6,7,8,9]
    else: # all
        months = range(1,13)
    if size=='large':
        criteria = getLargeOnesCriteria()
    elif size=='largeso55S':
        criteria = getLargeOnesSo55SCriteria()
    elif size=='smallso55S':
        criteria = getSmallOnesSo55SCriteria()
    elif size=='small':
        criteria = getSmallOnesCriteria()
    else:
        criteria = getGeneralCriteria()
    attr = numpy.array(fetchAttrLst('locations',locattrname,criteria=criteria,\
                                    years=years,months=months,colidx=colidx))
    return attr

def fetchCyclones(criteria=None,cycfunc=None,years=range(2001,2010),months=range(1,13),\
                  computer='nomet'):
    """ works now
    """
    cycs = []
    for year in years:
        for month in months:
            dbfile = _getDbFile(year,month,computer=computer)
            if dbfile is None: continue
            cycs.extend(fetchCycsFromDb(dbfile,criteria,cycfunc))
    return cycs

def getCycLocAttrs(cycs,locattrnames,locattrfunc = lambda x: x):
    locattrs = {}
    for cyc in cycs:
        for locattrname in locattrnames:
            locattrlst = [getattr(loc,locattrname) for loc in cyc.locations if hasattr(loc,locattrname)]
            try:
                locattrs[locattrname].extend(locattrlst)
            except:
                locattrs[locattrname] = locattrlst
    return locattrs

def plotHistogram(data,xlabel,legends,figfile,\
                  hrange=None,bins=20,normed=True,\
                      xlim=None,ylim=None,legloc=0,\
                      xfmt=None,yfmt=None):
    import matplotlib
    matplotlib.rcParams['lines.color'] = '0.0'
    import matplotlib.axes
    matplotlib.axes.set_default_color_cycle(map(str,numpy.linspace(0,1,len(legends))))
    #matplotlib.use('Agg')
    import pylab
    pylab.clf()
    pylab.clf()
    p1,b1,h1 = pylab.hist(data,bins=bins,range=hrange,normed=normed,align='mid')
    pylab.ylabel(r'f/$\Delta$bin') if normed else pylab.ylabel("count")
    pylab.xlabel(xlabel)
    pylab.legend([h1[i][0] for i in range(len(legends))],legends,loc=legloc)
    if xlim is not None:
        pylab.xlim(xlim)
    if ylim is not None:
        pylab.ylim(ylim)
    if xfmt is not None:
        fmt = matplotlib.ticker.FormatStrFormatter(xfmt)
        ax = pylab.gca()
        ax.xaxis.set_major_formatter(fmt)
    if yfmt is not None:
        fmt = matplotlib.ticker.FormatStrFormatter(yfmt)
        ax = pylab.gca()
        ax.yaxis.set_major_formatter(fmt)
    pylab.draw(); pylab.savefig(figfile)

def smooth2Dmatrix(a):
    """
    Computes mean of surrounding 9 points
    of matrix a
    """
    b = 1.0*a # ensure a is float
    if a.ndim!=2: return b
    for i in range(1,a.shape[0]-1):
        for j in range(1,a.shape[1]-1):
            b[i,j] = numpy.mean(a[i-1:i+2,j-1:j+2])
            # possible faster way if critical
            #b[i,1:-1] = [numpy.mean(a[i-1:i+2,j-1:j+2]) for j in range(1,a.shape[1]-1)]
    # normalise sum
    b *= a.sum()/b.sum()
    return b

def get2Dcount(a1,a2,n=50,smooth=False):
    """
    compute counts to a 2D (a1,a2) grid
    NOTE: f2d[ax1,ax2]
    """
    axs1 = numpy.linspace(a1.min(),a1.max()+0.1,n+1)
    axs2 = numpy.linspace(a2.min(),a2.max()+0.1,n)
    f2d = numpy.zeros((n+1,n),dtype='i')
    for i1,ax1 in enumerate(axs1[:-1]):
        for i2,ax2 in enumerate(axs2[:-1]):
            f2d[i1,i2] = len(numpy.where(\
            (a1>=ax1) & (a1<axs1[i1+1]) & \
            (a2>=ax2) & (a2<axs2[i2+1]))[0])
    if smooth: f2d = smooth2Dmatrix(f2d)        
    return f2d,axs1,axs2
#    return f2d,0.5*(axs1[:-1]+axs1[1:]),0.5*(axs2[:-1]+axs2[1:])

def getNormalised2Dcount(a1,a2,n=50):
    """
    Counts will be normalised so that their sum*dx*dy is 1.
    Effectively this returns 2d probability densities
    """
    f2d,axs1,axs2 = get2Dcount(a1,a2,n=n)
    d1 = a1[1]-a1[0]; d2 = a2[1]-a2[0]
    norm = numpy.sum(f2d)*dx*dy
    return f2d/norm,axs1,axs2

def printCorrCoef(x,y,xname='x',yname='y',set='all'):
    from common_functions import corrcoef
    c,u,l = corrcoef(x,y)
    if numpy.abs(c[0,1])>=0.4:
        print "%s & %s & %s & %4.3f \\" % (set,xname,yname,c[0,1])
    return "%s:[r,lb,ub](95 pct.): [%f,%f,%f]" % (set,c[0,1],u[0,1],l[0,1])

def getLabel(varname):
    if varname=='lifetime_in_secs':
        return "lifetime [s]"
    elif varname=='voyage':
        return "voyage [km]"
    elif varname=='beeline':
        return "beeline [km]"
    elif varname.split('_')[-1]=='c':
        varlab = "system intensity [hPa (deg lat)$^{-2}$]"
    elif varname.split('_')[-1]=='rd':
        if varname=='max_c_rd':
            return "radius when max. intensity [deg.lat]"
        varlab = "system radius [deg.lat]"
    elif varname.split('_')[-1]=='dp':
        varlab = "system depth [hPa]"
    elif varname.split('_')[-1]=='p':
        varlab = "pressure [hPa]"
    elif varname.split('_')[-1]=='wp':
        varlab = "speed [m/s]"
    elif varname.split('_')[-1]=='ws':
        varlab = "wind [m/s]"
    elif varname.split('_')[-1]=='vor':
        varlab =  "vorticity [1/s] 10$^{-3}$"
    elif varname.split('_')[-1]=='div':
        varlab = "divergence [1/s] 10$^{-3}$"
    elif varname.split('_')[-1]=='sst':
        if len(varname.split('_'))<2:
            varlab = "SST [K]"
        else:
            varlab = "SST gradient [K/m] 10${-3}$"
    elif varname.split('_')[-1]=='t' and varname.split('_')[-2]=='ground':
        varlab = "Tskin [K]"
    elif varname.split('_')[-1]=='rain':
        varlab = "rain [mm/3h]"
    elif varname.split('_')[-1]=='shflux':
        varlab = "sensible heat [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='lhflux':
        varlab = "latent heat [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='swdown':
        varlab = "shortwave radiation down [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='swup':
        varlab = "shortwave radiation up [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='lwdown':
        varlab = "longwave radiation down [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='lwup':
        varlab = "longwave radiation up [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='te':
        varlab = "total energy [Wm$^{-2}$]"
    elif varname.split('_')[-1]=='ust':
        varlab = "friction velocity [m/s]"
    elif varname.split('_')[-1]=='cice':
        varlab = "sea-ice concentration"
    elif varname.split('_')[-1]=='terrain':
        varlab = "terrain [m]"
    elif varname.split('_')[-1]=='y':
        varlab = "latitude [deg.]"
    else:
        varlab = varname.split('_')[-1]

    if varname.split('_')[0]=='sum':
        return 'mean '+varlab
    elif varname.split('_')[0]=='max':
        return 'max. '+varlab
    elif varname.split('_')[0]=='min':
        return 'min. '+varlab
    elif varname.split('_')[0]=='range':
        return 'max-min. '+varlab
    else:
        return varlab

def plotContourWithPoints(cdat,x,y,xlabel,ylabel,figfile,title,clevs,\
                              xp=None,yp=None,vmax=None):
    import matplotlib.pyplot as plt
    plt.clf()
    fig = plt.figure()
    plot = fig.add_subplot(111)
    plotContourPanelWithPoints(plot,cdat,x,y,xlabel,ylabel,title,clevs,\
                               xp=xp,yp=yp,vmax=vmax)
    plt.draw()
    plt.savefig(figfile)

def plotContourPanelWithPoints(plot,cdat,x,y,xlabel,ylabel,title,clevs,\
                               xp=None,yp=None,vmax=None,xlim=None,ylim=None):
    if vmax is None:
        if cdat.max()>=100:
            vmax= 100*(cdat.max()/100)
        elif cdat.max()<=10:
            vmax= 10
        else:
            vmax= 10*(cdat.max()/10)
    #X,Y = numpy.meshgrid(x,y)
    spts = numpy.where(cdat>=clevs[0])
    imx = numpy.maximum(spts[0].max()+1,5)
    imy = numpy.maximum(spts[1].max()+1,5)
    inx = numpy.maximum(spts[0].min()-1,0)
    iny = numpy.maximum(spts[1].min()-1,0)
    if xp is not None:
        plt.plot(xp,yp,'.',color='gray',alpha=0.01) # WithPoints
    #im = plot.contour(x[inx:imx],y[iny:imy],cdat[inx:imx,iny:imy].T,clevs,colors='k')
    im = plot.contour(x,y,cdat.T,clevs,colors='k')
    plot.clabel(im,fmt='%d',fontsize=10,inline=False)
    if xlim is None:
        plot.set_xlim(x[inx],x[imx])
    else:
        plot.set_xlim(xlim)
    if ylim is None:
        plot.set_ylim(y[iny],y[imy])
    else:
        plot.set_ylim(ylim)        
    plot.set_title(title)
    plot.set_xlabel(xlabel)
    plot.set_ylabel(ylabel)
    plot.yaxis.set_label_coords(-0.2, 0.5)

def plotRegressionPcolor(cdat,x,y,xlabel,ylabel,figfile,title,vmax=None,lut=10):
    import matplotlib.pyplot as plt
    if vmax is None:
        if cdat.max()>=100:
            vmax= 100*(cdat.max()/100)
        elif cdat.max()<=10:
            vmax= 10
        else:
            vmax= 10*(cdat.max()/10)
    plt.clf()
    fig = plt.figure()
    plot = fig.add_subplot(111)
    #X,Y = numpy.meshgrid(x,y)
    imx = numpy.maximum(numpy.where(cdat>vmax/10)[0].max()+1,10)
    imy = numpy.maximum(numpy.where(cdat>vmax/10)[1].max()+1,10)
    inx = numpy.maximum(numpy.where(cdat>vmax/10)[0].min()-1,0)
    iny = numpy.maximum(numpy.where(cdat>vmax/10)[1].min()-1,0)
    im = plot.pcolormesh(x[inx:imx+1],y[iny:imy+1],cdat[inx:imx,iny:imy].T,cmap=plt.get_cmap('Greys',lut),vmin=0,vmax=vmax)
    fig.colorbar(im,extend='max')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.draw()
    plt.savefig(figfile)

def _fetchCycLocsFromDb(dbfile,criteria,cycfunc=None):
    cycs = fetchCycsFromDb(dbfile,criteria,cycfunc)
    cyclocs = []
    for cyc in cycs:
        cycloc = CycloneLocs()
        for location in cyc.locations:
            cycloc.lon.append(location.x); cycloc.lat.append(location.y)
        cyclocs.append(cycloc)
    return cyclocs

def plotSpaghetti(figfile,criteria,years=range(2001,2010),months=range(1,13),cycfunc=None):
    import pylab
    from mpl_toolkits.basemap import Basemap
    m = Basemap(projection='spstere',lon_0=180.,boundinglat=-40.)
    cycs = fetchCyclones(criteria=criteria,cycfunc=cycfunc,years=years,months=months)
    for cyc in cycs:
        lon = [loc.x for loc in cyc.locations]
        lat = [loc.y for loc in cyc.locations]
        x,y = m(lon,lat)
        m.plot(x,y,'-')
    m.drawcoastlines()
    m.drawmeridians([0,45,90,135,180,225,270,315],labels=[1,1,1,1])
    m.drawparallels([-50,-60,-70,-80],labels=[1,1,1,1])
    pylab.draw()
    pylab.savefig(figfile)

def plotPcolor(m,x,y,data,figfile):
    import pylab
    m.pcolor(x,y,data)
    m.drawcoastlines()
    m.drawmeridians([0,45,90,135,180,225,270,315],labels=[1,1,1,1])
    m.drawparallels([-50,-60,-70,-80],labels=[1,1,1,1])
    pylab.draw()
    pylab.savefig(figfile)

def _compTrackDensityFromDb(dbfile,criteria,cycfunc=None,hemi='sh'):
    r2d, s2d, lon2d, lat2d = EASEGrid(hemi=hemi)
    cycs = fetchCycsFromDb(dbfile,criteria,cycfunc)
    tdensity = numpy.zeros(r2d.shape)
    for cyc in cycs:
        rvec, svec, uidx = EASEGridUniqLocs(cyc,hemi=hemi)
        for i in uidx:
            tdensity[i] += 1
    return tdensity

def compTrackDensity(figfile,criteria,years=range(1979,2016),months=range(1,13),cycfunc=None,hemi='sh'):
    r2d, s2d, lon2d, lat2d = EASEGrid(hemi=hemi)
    tdensity = numpy.zeros(r2d.shape)
    cycs = fetchCyclones(criteria=criteria,cycfunc=cycfunc,years=years,months=months)
    for cyc in cycs:
        rvec, svec, uidx = EASEGridUniqLocs(cyc,hemi=hemi)
        for i in uidx:
            tdensity[i] += 1
    return tdensity

def prcValsStr(data,prcs=(5,10,25,50,75,90,95)):
    from pylab import prctile
    return reduce(lambda x,y: x+y, map(lambda x: "%g "% x, prctile(data,prcs)))

def EASETrack(system,hemi='sh'):
    """
    Return system locations in EASE coordinates
    """
    try:
        import ezlhconv
    except:
        print "Install ezlhconv!"
        sys.exit(0)
    rvec = []; svec = []
    hc = 'Sl' if hemi=='sh' else 'Nl'
    for location in system.locations:
        status, r, s = ezlhconv.ezlh_convert(hc,location.y,location.x)
        if status!=0: print "ezlh_convert failed!"; sys.exit(1)
        rvec.append(r); svec.append(s)
    return numpy.array(rvec),numpy.array(svec)

def EASEGrid(hemi='sh'):
    try:
        import ezlhconv
    except:
        print "Install ezlhconv!"
        sys.exit(0)
    hc = 'Sl' if hemi=='sh' else 'Nl'
    # works with 25km grid (Sl)->25*10=250km cellsize
    # r1d = numpy.arange(200.,540.,10.,dtype='Float32')
    # s1d = numpy.arange(200.,540.,10.,dtype='Float32')
    r1d = numpy.arange(100.,640.,10.,dtype='Float32')
    s1d = numpy.arange(100.,640.,10.,dtype='Float32')
    nx = len(r1d)
    ny = len(s1d)
    r2d = numpy.tile(r1d,(ny,1))
    s2d = numpy.transpose(numpy.tile(s1d,(nx,1)))
    lon2d = numpy.zeros(r2d.shape,dtype='Float32')
    lat2d = numpy.zeros(r2d.shape,dtype='Float32')
    for j in range(r2d.shape[1]):
        for i in range(r2d.shape[0]):
            status, lat2d[i,j], lon2d[i,j]=\
                    ezlhconv.ezlh_inverse(hc,r2d[i,j],s2d[i,i])
            if status!=0: print "ezlh_inverse failed!"; sys.exit(1)
    return r2d, s2d, lon2d, lat2d

def EASEGridNearestPoints(system,hemi='sh'):
    """
    List of nearest EASE grid points of system locations
    """
    r2d, s2d, lon2d, lat2d = EASEGrid(hemi=hemi)
    rvec, svec = EASETrack(system,hemi=hemi)
    nrvec=[]; nsvec = []; nidx = []
    for idx in range(1,len(rvec)):
        dst = numpy.power(rvec[idx]-r2d,2)+numpy.power(svec[idx]-s2d,2)
        minpos = (numpy.where(dst==numpy.min(dst))[0][0],numpy.where(dst==numpy.min(dst))[1][0])
        nrvec.append(r2d[minpos]); nsvec.append(s2d[minpos]); nidx.append(minpos)
    return numpy.array(nrvec), numpy.array(nsvec), nidx

def EASEGridUniqLocs(system,hemi='sh'):
    """
    Return unique system locations in EASE grid
    """
    rvec, svec, nidx = EASEGridNearestPoints(system,hemi=hemi)
    urvec=[]; usvec = []; uidx = []
    urvec.append(rvec[0]); usvec.append(svec[0]); uidx.append(nidx[0])
    for idx in range(1,len(rvec)):
        r = rvec[idx]; s = svec[idx]
        already = False
        for udx in range(len(urvec)):
            if r==urvec[udx] and s==usvec[udx]: already = True
        if not already:
            urvec.append(r)
            usvec.append(s)
            uidx.append(nidx[idx])
    return numpy.array(urvec),numpy.array(usvec), uidx

###
if __name__=="__main__":
    import pylab, numpy
    #max_radii = numpy.array(fetchAttrLst('max_rd'))
    lons = fetchAttrLst('locations',locattrname='shtfl',\
                        years=[1979],months=[1],\
                        computer=None)
    print len(lons)
    print "Finished"
