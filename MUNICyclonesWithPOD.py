#!/usr/bin/env python
"""
A library to read trkdat files:
- to establish cyclone object
-- consisted of cyclone locations
- to establish cyclone location object

NOTE: This version can be used to store Cyclones into a sqlite
db instance.
"""

__Author__  = 'P. Uotila, GES/MU <Petteri.Uotila@arts.monash.edu.au>'
__Date__    = '28/04/08'
__Version__ = 0.1

import sys,os,string,re,numpy,cPickle,gzip,math
from datetime import datetime, timedelta
sys.path.append(os.path.join(os.environ['HOME'],'lib/python2.7/site-packages/pod-0.54.2-py2.7.egg'))
try:
    import pod  # to store systems into a sql db
except:
    print "Install pod! e.g. easy_install --prefix=$HOME pod"
    sys.exit(0)

# globals
Rearth = 6367.
SeasonMonths = {'djf':[12,1,2],'mam':[3,4,5],'jja':[6,7,8],'son':[9,10,11],\
                'all':range(1,13)}
MonthSeason  = ['djf','djf','mam','mam','mam','jja','jja','jja','son','son','son','djf']

class CycloneLoc(pod.Object):
    """
    one line from trkdat file
    """
    x = pod.typed.Float(index = True)
    y = pod.typed.Float(index = True)
    date = pod.typed.Object(index = True)
    # perhaps add other floats like metvars but do not index

    def __init__(self,line,genesis=False,lysis=False,years=[2000],**kwargs):
        pod.Object.__init__(self, **kwargs)
        vals = string.split(line)
        # Decimal time after da0,hr0
        self.t  = float(vals[0])
        # Date
        self.da = int(vals[1])
        twodigyr = self.da/10000
        centuries = list(set([year/100 for year in years]))
        self.year = twodigyr + centuries[0]*100 # if data covers one century 
        if len(centuries)>1:
            for cyear in years:
                if twodigyr==cyear%100: self.year = cyear # first possible twodigit year
        self.month = (self.da%10000)/100
        self.day = self.da%100
        # Hour
        self.hr = int(vals[2])
        self.hour = self.hr/100
        self.min = self.hr%100
        self.date = datetime(self.year,self.month,self.day,self.hour,self.min)
        # Status
        self.stat = int(vals[3])
        # Cyclone no. in cyclone file
        self.k = int(vals[4])
        # Open/closed status: 0 (strong closed), 1 (strong open), 10 (weak closed), 11 (weak open).
        self.opt = int(vals[5])
        # Matching probability
        self.q = float(vals[6])
        # Longitude
        self.x = float(vals[7])
        # Latitude
        self.y = float(vals[8])
        # Central pressure
        self.p = float(vals[9])
        # Delsq central pressure
        # i.e. intensity
        self.c = float(vals[10])
        # Cyclone depth
        self.dp = float(vals[11])
        # Cyclone radius
        self.rd = float(vals[12])
        # Steering E'wds velocity
        self.up = float(vals[13])
        # Steering N'wds velocity
        self.vp = float(vals[14])
        # Steering speed
        self.wp = math.sqrt(math.pow(self.up,2)+math.pow(self.vp,2))
        # true if first record
        self.genesis = genesis
        # true if last record
        self.lysis = lysis
        # associated SOM node column and row numbers for self.date
        self.SOMcol = None
        self.SOMrow = None
        # meteorologival vars from amps netcdf files or derived: (avg,std,max,min) tuple
        self.metvarnames = ['sst','ws','vor','div','ds_sst','rain','shflux','lhflux',\
                            'ust','lwdown','swdown','ground_t','ds_ground_t','lwup',\
                            'swup','te','albedo','terrain']
        for metvar in self.metvarnames:
            setattr(self,metvar,None)
        # CICE related variables (from NSIDC0079)
        self.cice = None # (avg,std,max,min) tuple

    def get_SOM_node_row_column(self,vislines=None,\
                                visfile="../som/pmsl/all/x4_y3/amps_cd01_2001010500_2009071321_pmsl.vis"):
        """
        Finds corresponding date from a vis-file
        """
        import common_functions as cf
        if vislines is None:
            vislines = numpy.loadtxt(visfile,dtype='int',skiprows=1)
        refdate = int(cf.date2label(self.date))
        sidx = numpy.where(vislines[:,2]==refdate)[0]
        if len(sidx)>0:
            self.SOMcol = vislines[sidx[0],0] # x
            self.SOMrow = vislines[sidx[0],1] # y

    def is_valid(self,season='all',SOM=True):
        if SOM:
            if self.SOMcol is None or self.SOMrow is None:
                return False
        if SeasonMonths[season].count(self.month):
            return True
        return False

    def read_nc_var(self,nc,varname,ncname):
        try:
            if varname=='lon' or varname=='lat' or varname=='terrain':
                return numpy.asarray(nc.variables[varname][:]) # already 2d
            else:
                return numpy.asarray(nc.variables[varname][0,:,:]) # 3d field (time=0,x,y)->2d
        except:
            print "Failed when reading %s from %s!" % (varname,ncname)
            sys.exit(1)

    def get_horizontal_resolution(self):
        """ return dx=dy for d01 in km
        """
        if self.year<2006:
            return 90.
        elif self.date>=datetime(2006,1,1) and self.date<datetime(2008,10,1,12):
            return 60.
        else:
            return 45.

    def read_meteorological_vars(self,ncname):
        import Scientific.IO.NetCDF as nc
        import common_functions as cf
        vars = {}; mval = {}
        if not os.path.isfile(ncname):
            print "WARNING: %s is not a file!" % ncname; return None, None
        try:
            ncf = nc.NetCDFFile(ncname,'r')
        except:
            print "WARNING: Couldn't open %s!" % ncname; return None, None

        for varname in ['lon','lat','terrain']:
            vars[varname] = self.read_nc_var(ncf,varname,ncname)
        mval['terrain'] = -999.999
        for varname in ['sst','u','v','vor','div','rain_non','rain_con','shflux','lhflux',\
                        'ust','lwdown','swdown','ground_t']:
            vars[varname] = self.read_nc_var(ncf,varname,ncname)
            if varname=='vor' or varname=='div':
                vars[varname] /= self.get_horizontal_resolution() # 10**3 1/s
            if varname.find('rain')>-1 and self.date<datetime(2006,1,1,0):
                vars[varname] *= 10. # change cm/3h -> mm/3h if mm5, wrf has mm/3h
            mval[varname] = getattr(ncf.variables[varname],'missing_value')
        ncf.close()
        # print "... closed %s." % ncname
        return vars, mval

    def add_cice_vars(self):
        from NSIDC0079 import NSIDC0079
        import common_functions as cf
        varname = 'cice'
        cice = NSIDC0079(self.date)
        if cice.data is not None:
            setattr(self,varname,(cice.areal_value(self.x,self.y,self.rd*111.5),\
                                  cice.areal_value(self.x,self.y,self.rd*111.5,numpy.var),\
                                  cice.areal_value(self.x,self.y,self.rd*111.5,numpy.max),\
                                  cice.areal_value(self.x,self.y,self.rd*111.5,numpy.min)))
        else:
            # read sea_ice*100 from wrf output if possible
            try:
                import Scientific.IO.NetCDF as nc
                nc_dir='/nfs/5/home/rclimateuser/puotila/amps/netcdf/'
                ncname = cf.form_ncname(self.date,nc_dir=nc_dir)
                ncf = nc.NetCDFFile(ncname,'r')
                var_in = self.read_nc_var(ncf,'sea_ice',ncname)
                ncf.close()
                var_out = (100*self.interpolate_variable(var_in),\
                           100*self.interpolate_variable(var_in,func=numpy.var),\
                           100*self.interpolate_variable(var_in,func=numpy.max),\
                           100*self.interpolate_variable(var_in,func=numpy.min))
                setattr(self,varname,var_out)
            except:
                # if everything fails:
                print "WARNING: Couldn't open %s!" % ncname
                setattr(self,varname,(-100.,-100.,-100.,-100.))

    def get_albedo(self,ground_t=None):
        """ A really simple approach.
        AMPS has much more sophisticated
        """
        if ground_t is None: return None
        albedo = numpy.zeros(ground_t.shape,'f')+0.1 # open water
        # if Tsurf < -1.8C
        albedo[numpy.where(ground_t<271.35)] = 0.8 # snow covered ice
        return albedo

    def get_lwup(self,ground_t=None):
        if ground_t is None: return None
        return 5.6704e-8*numpy.power(ground_t,4)

    def get_ds_temp(self,var,missing_value=-999.999):
        ds = self.get_horizontal_resolution() # km
        dx_var = numpy.zeros(var.shape,dtype='float')
        dx_var[1:-1,:] = 0.5*(var[2:,:]-var[:-2,:])/ds # 10**3 K/m
        dy_var = numpy.zeros(var.shape,dtype='float')
        dy_var[:,1:-1] = 0.5*(var[:,2:]-var[:,:-2])/ds
        return numpy.sqrt(dx_var**2 + dy_var**2)        

    def interpolate_variable(self,val_in,func=numpy.average):
        """
        In polar stereographic grid
        """
        try:
            from PolarStereoAMPS import PolarStereoAMPS
        except:
            print "Install PolarStereoAMPS!"
            sys.exit(0)
        psamps = PolarStereoAMPS(self.date)
        # default is to take areal average within cyclone radius
        # can change that with func=function parameter
        return psamps.areal_value(val_in,self.x,self.y,self.rd*111.500,func=func)

    def add_meteorological_vars(self):
        import time, socket
        import common_functions as cf
        if re.search('muskox',socket.gethostname()):
            nc_dir='/data2/puotila/SOCS/amps/netcdf/'
        else:
            nc_dir='/nfs/5/home/rclimateuser/puotila/amps/netcdf/'
        ncname = cf.form_ncname(self.date,nc_dir=nc_dir)
        vars_in, mval = self.read_meteorological_vars(ncname)
        if len(vars_in)==0:
            print "Problems when opening %s ... skipping !" % ncname
            return
        for varname in self.metvarnames:
            if varname in ['ds_sst','ds_ground_t']:
                var_in_name = varname.split('_',1)[-1]
                missing_value=mval[var_in_name]
                var_in = self.get_ds_temp(vars_in[var_in_name],missing_value=missing_value)
            elif varname=='ws':
                missing_value=mval['u']
                u = vars_in['u']; v = vars_in['v']
                var_in = numpy.sqrt(u**2 + v**2)
            elif varname=='rain':
                missing_value=mval['rain_non']
                var_in = vars_in['rain_non']+vars_in['rain_con']
            elif varname=='albedo':
                missing_value=mval['ground_t']
                var_in = self.get_albedo(vars_in['ground_t'])
            elif varname=='swup':
                missing_value=mval['swdown']
                albedo = self.get_albedo(vars_in['ground_t'])
                var_in = albedo*vars_in['swdown']
            elif varname=='lwup':
                missing_value=mval['ground_t']
                var_in = self.get_lwup(vars_in['ground_t'])
            elif varname=='te':
                missing_value=mval['swdown']
                lwup = self.get_lwup(vars_in['ground_t'])
                albedo = self.get_albedo(vars_in['ground_t'])
                swup = albedo*vars_in['swdown']
                var_in = lwup-vars_in['lwdown']+vars_in['shflux']+\
                    vars_in['lhflux']+swup-vars_in['swdown']
                # positive turbulent fluxes are upward
            else:
                missing_value=mval[varname]
                var_in = vars_in[varname]
            var_out = (self.interpolate_variable(var_in),\
                           self.interpolate_variable(var_in,func=numpy.var),\
                           self.interpolate_variable(var_in,func=numpy.max),\
                           self.interpolate_variable(var_in,func=numpy.min))
            setattr(self,varname,var_out)

    def CloseBoundary(self,halo=1):
        """ Returns True if location is 
        inside the halo zone from domain boundaries
        """
        try:
            from PolarStereoAMPS import PolarStereoAMPS
        except:
            print "Install PolarStereoAMPS!"
            sys.exit(0)
        import common_functions as cf
        import GeoInterpolate as GI
        psamps = PolarStereoAMPS(self.date)
        r,c = psamps.ll2ij(self.x,self.y)
        if r-1-halo<=0: return True
        if c-1-halo<=0: return True
        if r>psamps.nx-1-halo: return True
        if c>psamps.ny-1-halo: return True
        return False
        # old (slow) part below 
        import time, socket
        if re.search('muskox',socket.gethostname()):
            nc_dir='/data2/puotila/SOCS/amps/netcdf/'
        else:
            nc_dir='/nfs/5/home/rclimateuser/puotila/amps/netcdf/'
        ncname = cf.form_ncname(self.date,nc_dir=nc_dir)
        lon2d,lat2d = cf.get_nc_coords(ncname)
        n1,n2 = lon2d.shape
        idxs = GI.GeoInterpolate(lon2d,lat2d,\
                                     numpy.array([[self.x]]),numpy.array([[self.y]]),\
                                     radius=100.0e3,method='nearest',f90=True)
        #NOTE: if f90, indices go from 1 to N (if not, from 0 to N-1)
        if (idxs.weights[0][0][0][0]-2-halo)<0: return True
        if (idxs.weights[0][0][0][1]-2-halo)<0: return True
        if idxs.weights[0][0][0][0]>(n1-1-halo): return True
        if idxs.weights[0][0][0][1]>(n2-1-halo): return True
        return False

class Cyclone(pod.Object):
    """
    A cyclone detected by MUNI scheme in a trkdat file
    - can contain one or more MUNICycloneLocs
    """
    lifetime_in_secs = pod.typed.Float(index = True)
    voyage           = pod.typed.Float(index = True)
    beeline          = pod.typed.Float(index = True)
    max_c            = pod.typed.Float(index = True)
    max_rd           = pod.typed.Float(index = True)
    max_c_rd         = pod.typed.Float(index = True)
    max_dp           = pod.typed.Float(index = True)
    boundary         = pod.typed.Int(index = True) # 1 if hits domain boundary
    bomb             = pod.typed.Int(index = True) # 1 if bomb
    month            = pod.typed.Int(index = True) # genesis
    year             = pod.typed.Int(index = True) # genesis
    label            = pod.typed.Int(index = True) # track number
    gen_lon          = pod.typed.Float(index = True)
    gen_lat          = pod.typed.Float(index = True)
    lys_lon          = pod.typed.Float(index = True)
    lys_lat          = pod.typed.Float(index = True)
    min_p            = pod.typed.Float(index = True)
    max_wp           = pod.typed.Float(index = True)
    # (avg,std,max,min):
    max_terrain      = pod.typed.Float(index = True)
    min_terrain      = pod.typed.Float(index = True)
    min_ground_t     = pod.typed.Float(index = True)
    min_vor          = pod.typed.Float(index = True)
    min_div          = pod.typed.Float(index = True)
    sum_ds_sst       = pod.typed.Float(index = True)
    sum_ds_ground_t  = pod.typed.Float(index = True)
    sum_rain         = pod.typed.Float(index = True)
    sum_te           = pod.typed.Float(index = True)
    sum_ust          = pod.typed.Float(index = True)
    sum_ground_t     = pod.typed.Float(index = True)
    max_cice         = pod.typed.Float(index = True)
    min_cice         = pod.typed.Float(index = True)
    range_cice       = pod.typed.Float(index = True)

    def __init__(self,trackline,years=[2000],**kwargs):
        pod.Object.__init__(self, **kwargs)
        vals = string.split(trackline)
        self.locations = []
        pat = r"Track\s*(\d+):\s+stat\s+=\s+(\d+),\s+ifst\s+=\s+(\d+),\s+ilst\s+=\s+(\d+),\s+nit\s+=\s+(\d+)\s+(.{10})\s+-\s+(.{10})\."
        m = re.search(pat,trackline)
        if not m:
            print "%s doesnt match pattern!" % trackline; sys.exit(1)
        self.label          = int(m.group(1))
        self.nlocs          = int(m.group(5)) # how many locations this cyclone has
        # year is two digits only
        self.genesis        = self.str2date(m.group(6),years=years)
        lyears = [year for year in years if year>=self.genesis.year]
        self.lysis          = self.str2date(m.group(7),years=lyears)
        self.month          = self.genesis.month
        self.year           = self.genesis.year
        self.gen_lat        = None
        self.gen_lon        = None
        self.lys_lat        = None
        self.lys_lon        = None
        self.lifetime       = self.lysis-self.genesis
        #self.lifetime_in_secs = self.get_lifetime_in_secs()
        self.lifetime_in_secs = None
        self.years          = range(self.genesis.year,self.lysis.year+1)
        self.beeline        = None
        self.voyage         = None
        self.boundary       = 0
        self.bomb           = 0
        # (avg,std,max,min)
        self.statvars = ['c','rd','dp','p','wp',\
                             'sst','ws','vor','div','ds_sst','rain','shflux','lhflux',\
                             'ust','lwdown','swdown','ground_t','lwup','swup','te','albedo',\
                             'terrain','cice']
        for var in self.statvars:
            setattr(self,var,None)
        # index vars for quick queries (ADD min_terrain!!)
        self.indexvars = ['max_c','max_rd','max_c_rd','max_dp','min_p','max_wp',\
                          'max_terrain','min_terrain','min_ground_t','max_ws','min_vor','min_div',\
                          'sum_ds_sst','sum_ds_sst','sum_sst','sum_ground_t','sum_rain','sum_shflux',\
                          'sum_lhflux','sum_ust',\
                          'sum_lwdown','sum_lwup','sum_swdown','sum_swup','sum_te',\
                          'max_cice','min_cice','range_cice']
        for var in self.indexvars:
            setattr(self,var,None)

    def get_lifetime_in_secs(self):
        import common_functions as cf
        return cf.timedelta_in_days(self.lifetime)*86400

    def str2date(self,str,years=[]):
        da = int(str[:6]); hr = int(str[6:])
        twodigyr = int(str[:6])/10000
        if len(years)==0: years = self.years
        centuries = list(set([year/100 for year in years]))
        year = twodigyr + centuries[0]*100 # if data covers one century 
        if len(centuries)>1:
            for cyear in years:
                if twodigyr==cyear%100: year = cyear # first possible twodigit year
        # sometimes, like in Kevs ERA40, years been transformed from 1998->1918 etc
        #if year-years[-1]>=80: year += 80
        month = (da%10000)/100
        day = da%100
        hour = hr/100
        min = hr%100
        return datetime(year,month,day,hour,min)

    def initCycloneLoc(self,locationline,genesis,lysis):
        """
        A simple method and hence easy to override
        """
        return CycloneLoc(locationline,genesis=genesis,lysis=lysis,years=self.years)

    def add_location(self,locationline,vislines=None,genesis=False,lysis=False,\
                     meteorology=True,SOM=True,cice=True):
        #location = CycloneLoc(locationline,genesis=genesis,lysis=lysis,years=self.years)
        location = self.initCycloneLoc(locationline,genesis,lysis)
        if SOM:
            location.get_SOM_node_row_column(vislines=vislines)
        if meteorology:
            location.add_meteorological_vars()
        if cice:
            location.add_cice_vars()
        if location.CloseBoundary(): 
            setattr(self,'boundary',1)
        self.locations.append(location)

    def get_beeline(self):
        """
        Compute distance [km] between first and last location
        """
        import common_functions as cf
        R = 6367.
        if len(self.locations)==0:
            beeline = None
        elif len(self.locations)==1:
            beeline = 0.0
        else:
            beeline = R*cf.haversine(self.locations[0].x,self.locations[-1].x,\
                                     self.locations[0].y,self.locations[-1].y)
        return beeline

    def set_beeline(self):
        setattr(self,'beeline',self.get_beeline())

    def get_voyage(self):
        """
        Compute total length of system trajectory [km]
        """
        import common_functions as cf
        if len(self.locations)==0:
            voyage = None
        else:
            voyage = 0.0
            for lidx in range(1,len(self.locations)):
                voyage += Rearth*cf.haversine(self.locations[lidx].x,self.locations[lidx-1].x,\
                                              self.locations[lidx].y,self.locations[lidx-1].y)
        return voyage

    def set_voyage(self):
        setattr(self,'voyage',self.get_voyage())

    def get_location_property(self,propertyname):
        """
        Return array of cyclone property from its locations
        NOTE: Array is a list of tuples (avg,std,max,min) for metvars and cice
        """
        array = []
        if ['date','genesis','lysis'].count(propertyname)==1:
            for location in self.locations:
                array.append(getattr(location,propertyname))
            return array
        elif ['q','x','y','p','c','dp','rd','up','vp','wp','ws','ground_t',\
              'sst','ds_sst','vor','div','rain','ust','shflux','lhflux',\
              'lwdown','swdown','swup','lwup','te','terrain','cice','albedo'].count(propertyname)==1:
            for location in self.locations:
                array.append(getattr(location,propertyname))
            return numpy.array(array,dtype='float')
        elif ['stat','k','opt','SOMcol','SOMrow'].count(propertyname)==1:
            for location in self.locations:
                array.append(getattr(location,propertyname))
            return numpy.array(array,dtype='int')
        else:
            print "WARNING: Cant recognise attribute %s !" % propertyname
            return numpy.array([None])

    def func_location_property(self,propertyname,func=lambda x: x,col=0):
        try:
            prop = self.get_location_property(propertyname)
            if numpy.ndim(prop)==1 or col==-1:        
                return func(prop)
            else:
                return func(prop[:,col])                
        except:
            return None

    def location_property_stats(self,propertyname):
        prop = self.get_location_property(propertyname)
        if numpy.ndim(prop)==2:
            return numpy.sum(prop[:,0]),numpy.sum(prop[:,1]),\
                numpy.max(prop[:,2]),numpy.min(prop[:,3])
        else:
            return numpy.sum(prop),numpy.var(prop),numpy.max(prop),numpy.min(prop)

    def location_property_index(self,propertyname):
        funcstr = propertyname.split('_')[0]
        col = 0
        if funcstr=='max':
            func=numpy.max
            col = 2
        elif funcstr=='min':
            func=numpy.min
            col = 3
        elif funcstr=='sum':
            func=numpy.sum
        elif funcstr=='range':
            func=lambda x: numpy.max(x[:,2])-numpy.min(x[:,3])
            col = -1
        else:
            func=lambda x: x
        locpropertyname = propertyname.split('_',1)[1]
        return self.func_location_property(locpropertyname,func,col=col)

    def get_max_c_rd(self):
        c  = self.get_location_property('c')
        rd = self.get_location_property('rd')
        return float(rd[numpy.where(c==numpy.max(c))[0][-1]])

    def set_gen_lat(self):
        setattr(self,'gen_lat',self.func_location_property('y',lambda x: x[0]))

    def set_gen_lon(self):
        setattr(self,'gen_lon',self.func_location_property('x',lambda x: x[0]))

    def set_lys_lat(self):
        setattr(self,'lys_lat',self.func_location_property('y',lambda x: x[-1]))

    def set_lys_lon(self): 
        setattr(self,'lys_lon',self.func_location_property('x',lambda x: x[-1]))

    def is_bomb(self,dt=3,thold=24.):
        """
        set bomb flag=1 if pressure tendency >24hPa/24hrs
        """
        if self.lifetime_in_secs<86400.: return
        p = self.get_location_property('p')
        #if len(p)<24/dt: return
        for i in range(len(p)-1):
            for j in range(i+1,len(p)):
                dp = p[i]-p[j]
                if dp>=thold and j-i<=24/dt:
                    self.bomb=1

    def set_lifetime_in_secs(self):
        setattr(self,'lifetime_in_secs',self.get_lifetime_in_secs())

    def set_derived_attributes(self):
        """
        A wrapper setting system related values
        """
        self.set_lifetime_in_secs()
        self.set_beeline()
        self.set_voyage()
        self.set_gen_lat()
        self.set_gen_lon()
        self.set_lys_lat()
        self.set_lys_lon()
        for var in self.statvars:
            setattr(self,var,self.location_property_stats(var))
        for var in self.indexvars:
            if var=='max_c_rd':
                setattr(self,var,self.get_max_c_rd())
            else:
                setattr(self,var,self.location_property_index(var))
        self.is_bomb()

    def is_valid(self,season='all',year=0,months=[0]):
        """
        Test if system is valid:.
        - It occurs during the season
        """
        #if season=='djf':
        #    if numpy.abs(self.genesis.year-year)>1 and numpy.abs(self.lysis.year!=year)>1:
        #        return False
        #    if self.genesis.year>year and self.lysis.year>year:
        #        return False
        #else:
        #    if self.genesis.year!=year and self.lysis.year!=year:
        #        return False
        #if SeasonMonths[season].count(self.genesis.month) or \
        #   SeasonMonths[season].count(self.lysis.month):
        #    return True
        if SeasonMonths[season].count(self.genesis.month) and \
           months[0]==0 and \
           (self.genesis.year==year or year==0):
            return True
        if months.count(self.genesis.month) and (self.genesis.year==year or year==0):
            return True
        return False

class Cyclones:
    def __init__(self,model='amps',experiment='exp066',\
                 region='all',years=range(2001,2010),months=[0],season='all',\
                 quant='pmsl',visfile=None,meteorology=True,SOM=True,cice=True,\
                 **kwargs):
        # pod.Object.__init__(self, **kwargs)
        self.meteorology = meteorology
        if self.meteorology:
            suffix = "cpickle.gz"
        else:
            suffix = "nomet.cpickle.gz"
        self.savefile = "%s_%s_%s_%s_%s-%s.%s" % \
                        (string.upper(model),quant,experiment,season,years[0],years[-1],suffix)
        self.SOM = SOM
        self.cice = cice
        self.model = model
        self.experiment = experiment
        self.region = region
        self.years = years
        self.months = months
        self.quant = quant
        if season == 'all':
            self.seasons = ['djf','mam','jja','son']
        else:
            self.seasons = [season]
        if self.months[0]!=0: # override previous
            seasons = []
            for month in self.months:
                seasons.append(MonthSeason[month-1])
            self.seasons = set(seasons)
        self.cyclones = []
        self.vislines = None
        if visfile is not None and SOM:
            self.vislines = numpy.loadtxt(visfile,dtype='int',skiprows=1)

    def initCyclone(self,lines,li,year):
        """
        A simple method and hence easy to override
        """
        return Cyclone(lines[li],years=[year-1,year,year+1])

    def add_systems_from_trkfile(self,year=2001,season='all',trkdatfile=None,test=False):
        if trkdatfile is None:
            trkdatfile = "trkdat_%s_%04d-%04d.%s" % (self.quant,self.years[0],self.years[-1],self.model)
        print "Reading %s ..." % os.path.basename(trkdatfile)
        sys.stdout.flush()
        try:
            fp = open(trkdatfile)
            lines = fp.readlines()
            fp.close()
        except:
            return

        for li in range(len(lines)):
            m = re.search("Track",lines[li])
            if m:
                #system = Cyclone(lines[li],years=[year-1,year,year+1])
                system = self.initCyclone(lines,li,year)
                if not system.is_valid(season,year,self.months): continue
                genesis = True; lysis = False
                for ci in range(li+3,li+system.nlocs+3):
                    if ci==li+system.nlocs+3-1: lysis = True
                    system.add_location(lines[ci],vislines=self.vislines,\
                                        genesis=genesis,lysis=lysis,\
                                        meteorology=self.meteorology,\
                                        cice=self.cice,\
                                        SOM=self.SOM)
                    genesis = False
                system.set_derived_attributes()
                self.cyclones.append(system)
                if system.label%10==1:
                    print "System %d generated on %s processed at %s." % (system.label,system.genesis,datetime.now())
                    sys.stdout.flush()
                if test and system.label>10: break # testing
        return 0

    def add_systems_from_trkfiles(self,save=False,load=False,annual=True,trkpath=None,test=False):
        """
        Adds all systems according to self.years and self.seasons
        annual=True if processing annual files
        NEW: store to a sql db for quick queries!
        """
        import common_functions as cf
        if os.path.isfile(self.savefile) and load:
            gof = gzip.open(self.savefile,'r'); po = cPickle.Unpickler(gof)
            tmp = po.load(); gof.close()
            self.__dict__ = tmp.__dict__.copy()
            return
        print "Starting reading systems ..."
        if save:
            print " and then saving to %s." % self.savefile
        if annual:
            trkdatfile = None
            for year in self.years:
                for season in self.seasons:
                    if self.model=='amps':
                        if self.months[0]==12 and season=='djf':
                            # find out what to do when len(self.months)>1
                            trkfilename = "trkdat_%s_%s.%s" % \
                                          (self.quant,cf.get_season_tstr(year+1,'djf'),self.model)
                        else:
                            trkfilename = "trkdat_%s_%s.%s" % \
                                          (self.quant,cf.get_season_tstr(year,season),self.model)
                        trkdatfile = os.path.join(self.model,self.experiment,trkfilename)
                    self.add_systems_from_trkfile(year,season,trkdatfile=trkdatfile)
            if len(self.seasons)==4 and self.months[0]==0 and \
                   self.model=='amps': # extracting whole year
                trkfilename = "trkdat_%s_%s.%s" % (self.quant,cf.get_season_tstr(year+1,'djf'),self.model)
                if trkpath is None:
                    trkpath = os.path.join(self.model,self.experiment)
                if os.path.isdir(trkpath):
                    trkdatfile = os.path.join(trkpath,trkfilename)
                else:
                    trkdatfile = trkfilename # trkdatfiles in cwd
                self.add_systems_from_trkfile(year,'djf',trkdatfile=trkdatfile) # include dec from this (AMPS) file
        else:
            self.add_systems_from_trkfile(test=test)
        if save:
            gof = gzip.open(self.savefile,'w'); po = cPickle.Pickler(gof)
            po.dump(self); gof.close()

class MonthlyDistributions:
    """
    Monthly distributions of system properties
    - idea is to use data structure for boxplots (matplotlib)
    - month associated with system is the month when system was born
    """
    def __init__(self):
        # asom = list of most common som node(s) for system(s)
        self.props = ['beeline','voyage','lifetime','nsom','asom']
        for prop in self.props:
            setattr(self,prop,[])
            for month in range(12):
                getattr(self,prop).append(numpy.array([],dtype='float'))

    def add_system(self,system):
        midx = system.locations[0].month-1
        for prop in self.props:
            if prop=='asom' or prop=='nsom':
                continue
            else:
                getattr(self,prop)[midx] = numpy.append(getattr(self,prop)[midx],getattr(system,prop))

class SOMStatistics:
    """
    Statistics per som node
    """
    def __init__(self,season='all',som_xdim=4,som_ydim=3):
        self.season = season
        if self.season=='all':
            self.seasons = ['djf','mam','jja','son']
        else:
            self.seasons = [self.season] 
        self.som_xdim = som_xdim
        self.som_ydim = som_ydim
        # works with 25km grid (Sl)->25*10=250km cellsize
        self.r1d = numpy.arange(200.,540.,10.,dtype='Float32')
        self.s1d = numpy.arange(200.,540.,10.,dtype='Float32')
        self.nx = len(self.r1d)
        self.ny = len(self.s1d)
        self.r2d = numpy.tile(self.r1d,(self.ny,1))
        self.s2d = numpy.transpose(numpy.tile(self.s1d,(self.nx,1)))
        self.compute_latlon2d()
        self.sd = {} # system density
        self.dp = {} # system depth
        self.p  = {} # system central pressure
        self.rd = {} # system radius
        self.c  = {} # system intensity
        self.fg = {} # cyclogenesis
        self.fl = {} # cyclolysis
        self.ws = {} # abs(ub+i*vb)
        self.tr = {} # trajectories (!not [yet] clear how to do!)
        self.nsom = {} # number of different som nodes system experienced (last location)
        self.lifetime = {} # lifetime (last location)
        self.beeline = {} # beeline (last location)
        self.voyage = {} # voyage (last location)
        self.proplist = ['sd','dp','p','rd','c','fg','fl','ws','lifetime','beeline','voyage','nsom'] # tr not added
        # add meteorological variables sw=|u+i*v|, ds_sst=|dx_sst+i*dy_sst|
        self.metvars = ['sst','sw','vor','div','ds_sst','rain','shflux','lhflux',\
                   'ust','lwdown','swdown']
        self.proplist.extend(self.metvars)
        for prop in self.proplist:
            if not hasattr(self,prop): setattr(self,prop,{}) 
            getattr(self,prop)['count'] = numpy.zeros((som_xdim,som_ydim,self.ny,self.nx),dtype='int32')
            getattr(self,prop)['sum'] = numpy.zeros((som_xdim,som_ydim,self.ny,self.nx),dtype='float64')
            getattr(self,prop)['sumsq'] = numpy.zeros((som_xdim,som_ydim,self.ny,self.nx),dtype='float64')
            getattr(self,prop)['min'] = numpy.zeros((som_xdim,som_ydim,self.ny,self.nx),dtype='float32')
            getattr(self,prop)['max'] = numpy.zeros((som_xdim,som_ydim,self.ny,self.nx),dtype='float32')
        # can add other properties and variance

    def compute_latlon2d(self):
        import ezlhconv
        self.lon2d = numpy.zeros(self.r2d.shape,dtype='Float32')
        self.lat2d = numpy.zeros(self.r2d.shape,dtype='Float32')
        for j in range(self.r2d.shape[1]):
            for i in range(self.r2d.shape[0]):
                status, self.lat2d[i,j], self.lon2d[i,j]=\
                        ezlhconv.ezlh_inverse('Sl',self.r2d[i,j],self.s2d[i,i])
                if status!=0: print "ezlh_inverse failed!"; sys.exit(1)

    def add_system(self,system,\
                   min_lifetime=3*3600.,max_mean_speed=50.,\
                   max_rd=90.):
        """
        To be added to statistics
        1) system's lifetime>=min_lifetime [secs] and
        2) its mean_speed<=max_mean_speed [m/s] and
        3) it has to have SOMcol and SOMrow != None
        4) its max_radius<max_rd
        """
        import common_functions as cf
        import ezlhconv
        lifetime_in_secs = cf.timedelta_in_days(system.lifetime)*86400
        sys_max_rd = system.get_max_radius()
        if lifetime_in_secs>=min_lifetime and system.get_max_radius()<max_rd:
            mean_speed = 1000*system.beeline/lifetime_in_secs
            if mean_speed<=max_mean_speed:
                for prop in self.proplist:
                    count= getattr(self,prop)['count']
                    sum  = getattr(self,prop)['sum']
                    sumsq= getattr(self,prop)['sumsq']
                    min= getattr(self,prop)['min']
                    max= getattr(self,prop)['max']
                    if ['sd','dp','p','rd','c','ws','sst','sw','vor','div','ds_sst',\
                        'rain','shflux','lhflux','ust','lwdown','swdown'].count(prop)==1: # properties per each location
                        for location in system.locations:
                            if location.is_valid(self.season):
                                nsum = sum[location.SOMcol,location.SOMrow,:,:]
                                nsumsq = sumsq[location.SOMcol,location.SOMrow,:,:]
                                ncount = count[location.SOMcol,location.SOMrow,:,:]
                                nmax = max[location.SOMcol,location.SOMrow,:,:]
                                nmin = min[location.SOMcol,location.SOMrow,:,:]
                                status, r, s = ezlhconv.ezlh_convert('Sl',location.y,location.x)
                                if status!=0: print "ezlh_convert failed!"; sys.exit(1)
                                dst = numpy.power(r-self.r2d,2)+numpy.power(s-self.s2d,2)
                                minpos = (numpy.where(dst==numpy.min(dst))[0][0],numpy.where(dst==numpy.min(dst))[1][0])
                                if prop=='sd':
                                    nsum[minpos] += 1.0
                                    ncount += 1.0
                                elif prop=='ws':
                                    ncount[minpos] += 1
                                    value = math.sqrt(math.pow(location.up,2.0)+\
                                                       math.pow(location.vp,2.0))
                                    nsum[minpos] += value
                                    nsumsq[minpos] += math.pow(value,2.0)
                                    if value>nmax[minpos]: nmax[minpos] = value
                                    if value<nmin[minpos] or nmin[minpos]==0.: nmin[minpos] = value
                                elif prop=='sw':
                                    ncount[minpos] += 1
                                    value = math.sqrt(math.pow(location.u,2.0)+\
                                                       math.pow(location.v,2.0))
                                    nsum[minpos] += value
                                    nsumsq[minpos] += math.pow(value,2.0)
                                    if value>nmax[minpos]: nmax[minpos] = value
                                    if value<nmin[minpos] or nmin[minpos]==0.: nmin[minpos] = value
                                elif prop=='ds_sst':
                                    ncount[minpos] += 1
                                    value = math.sqrt(math.pow(location.dx_sst,2.0)+\
                                                       math.pow(location.dy_sst,2.0))
                                    nsum[minpos] += value
                                    nsumsq[minpos] += math.pow(value,2.0)
                                    if value>nmax[minpos]: nmax[minpos] = value
                                    if value<nmin[minpos] or nmin[minpos]==0.: nmin[minpos] = value
                                else:
                                    value = getattr(location,prop)
                                    nsum[minpos] += value
                                    nsumsq[minpos] += math.pow(value,2)
                                    ncount[minpos] += 1
                                    if value>nmax[minpos]: nmax[minpos] = value
                                    if value<nmin[minpos] or nmin[minpos]==0.: nmin[minpos] = value
                    else: # properties per system
                        if prop=='fg':
                            location=system.locations[0]
                        else:
                            location=system.locations[-1]
                        if location.is_valid(self.season):
                            nsum = sum[location.SOMcol,location.SOMrow,:,:]
                            nsumsq = sumsq[location.SOMcol,location.SOMrow,:,:]
                            ncount = count[location.SOMcol,location.SOMrow,:,:]
                            status, r, s = ezlhconv.ezlh_convert('Sl',location.y,location.x)
                            if status!=0: print "ezlh_convert failed!"; sys.exit(1)
                            dst = numpy.power(r-self.r2d,2)+numpy.power(s-self.s2d,2)
                            minpos = (numpy.where(dst==numpy.min(dst))[0][0],numpy.where(dst==numpy.min(dst))[1][0])
                            if prop=='fg' or prop=='fl':
                                nsum[minpos] += 1.0
                                ncount += 1
                            elif prop=='lifetime':
                                nsum[minpos] += cf.timedelta_in_days(getattr(system,prop))
                                nsumsq[minpos] += math.pow(cf.timedelta_in_days(getattr(system,prop)),2)
                                ncount[minpos] += 1
                            elif prop=='nsom':
                                snodes = []
                                for location in system.locations:
                                    snodes.append([location.SOMcol,location.SOMrow])
                                nsum[minpos] += len(numpy.unique(numpy.asarray(snodes)))
                                nsumsq[minpos] += math.pow(len(numpy.unique(numpy.asarray(snodes))),2)
                                ncount[minpos] += 1
                            else:
                                nsum[minpos] += getattr(system,prop)
                                nsumsq[minpos] += math.pow(getattr(system,prop),2)
                                ncount[minpos] += 1
        return True # could be a useful place for debugging break points

class SOMPositions:
    """
    Look at SOM nodes (Sammon positions) per system variable:
    - ['beeline','voyage','lifetime']
    - boxs are:
    1. 1st node (fg)
    2. last node (fl)
    3. most common node (cn)
    4. longest duration node (ln)
    5. number of nodes (nn)
    6. number of unique nodes (nu)
    7. longest node transfer in Sammon space (sl)
    8. total node transfer in Sammon space (st)
    8. net node transfer in Sammon space (sn)
    """
    def __init__(self,\
                 sammon='../som/pmsl/all/x4_y3/amps_cd01_2001010500_2007042112_pmsl.sam'):
        self.systemprops = ['beeline','voyage','lifetime'] # properties per system
        self.somvars = ['fg','fl','cn','ln','nn','nu','sn','st','sl'] # som node variables per system property
        if os.path.isfile(sammon):
            self.sammon = sammon
            self.SammonCoords()
        else:
            print "Cant read %s!" % sammon; sys.exit(1)
        self.i, self.j, self.s = self.NodeIndices()
        # for each somvar we have a [s,beeline,voyage,lifetime] sets
        for var in self.somvars:
            setattr(self,var,numpy.array([[],[],[],[]],dtype='float'))

    def NodeIndices(self):
        """
        Return row and column indices and 1d sum index
        """
        i,j = numpy.indices((self.xdim,self.ydim))
        return i,j,i+j*self.xdim

    def SammonCoords(self):
        fp = open(self.sammon); header = string.split(fp.readline())
        self.xdim = int(header[2])
        self.ydim = int(header[3])
        self.sx = numpy.zeros((self.xdim,self.ydim),dtype='float')
        self.sy = numpy.zeros((self.xdim,self.ydim),dtype='float')
        for j in range(self.ydim):
            for i in range(self.xdim):
                line = string.split(fp.readline())
                self.sx[i,j] = float(line[0]); self.sy[i,j] = float(line[1])
        fp.close()

    def system_node_indices(self,system):
        """
        List of 1d indeces of som nodes of system
        """
        nids = numpy.array([],dtype='int')
        for location in system.locations:
            sr = location.SOMrow; sc = location.SOMcol
            spos = numpy.array([self.sx[sc,sr],self.sy[sc,sr]])
            nids = numpy.append(nids,self.s[sc,sr])
        return nids

    def s2ij(self,s):
        """
        Given 1d node index, returns 2d row,col indices
        """
        return self.i[numpy.where(self.s==s)][0],self.j[numpy.where(self.s==s)][0]

    def nids2sammon_distance(self,nid,basenode=0):
        """
        Distance in Sammon space from basenode to a node
        (when 1d node index=nid is known).
        """
        srb, scb = self.s2ij(basenode)
        bpos = numpy.asarray([self.sx[srb,scb],self.sy[srb,scb]])            
        sr, sc = self.s2ij(nid) 
        spos = numpy.asarray([self.sx[sr,sc],self.sy[sr,sc]])            
        dst  = math.sqrt(math.pow(spos[0]-bpos[0],2)+math.pow(spos[1]-bpos[1],2))
        return dst

    def most_common_nodes(self,nids,duration=8):
        """
        Returns list of most common nodes
        - only if duration is more than duration records
        """
        count = numpy.zeros(self.xdim*self.ydim,dtype='int')
        for nid in nids:
            count[nid] += 1
        if count.max()>duration:
            return numpy.where(count==count.max())[0]
        return None

    def system_sammon_distances(self,nids):
        """
        Return array of sammon distances 
        """
        sdsts = numpy.zeros([],dtype='float')
        for nid in nids:
            sdsts = numpy.append(sdsts,self.nids2sammon_distance(nid))
        return sdsts

    def total_sammon_distance(self,nids):
        return numpy.sum(self.system_sammon_distances(nids))

    def max_sammon_distance(self,nids):
        return numpy.max(self.system_sammon_distances(nids))

    def add_system(self,system):
        import common_functions as cf
        nids =  self.system_node_indices(system)
        lt_days = cf.timedelta_in_days(system.lifetime)
        if system.voyage<10.0: return # do not add systems that are probably stagnated
        for var in self.somvars:
            recs = []
            if var=='fg':
                recs.append(numpy.array([[nids[0]],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='fl':
                recs.append(numpy.array([[nids[-1]],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='nn':
                recs.append(numpy.array([[len(nids)],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='nu':
                recs.append(numpy.array([[len(numpy.unique(nids))],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='cn':
                cnis = self.most_common_nodes(nids)
                if cnis is not None:
                    for cni in cnis:
                        recs.append(numpy.array([[cni],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='ln':
                sln = len(numpy.where((numpy.diff(nids)==0))[0])
                recs.append(numpy.array([[sln],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='sn': # distance between first and last nids
                ssn = self.nids2sammon_distance(nids[0],nids[-1])
                recs.append(numpy.array([[ssn],[system.beeline],[system.voyage],[lt_days]]))
            elif var=='st':
                recs.append(numpy.array([[self.total_sammon_distance(nids)],[system.beeline],\
                                         [system.voyage],[lt_days]]))
            elif var=='sl':
                recs.append(numpy.array([[self.max_sammon_distance(nids)],[system.beeline],\
                                         [system.voyage],[lt_days]]))
            for rec in recs:
                setattr(self,var,numpy.append(getattr(self,var),rec,axis=1))

