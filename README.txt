1. Install pod
$ easy_install --prefix=$HOME pod
Note that you may need to modify sys.path.append() 
corresponding to the pod-egg installation path.

2. Get input file(s) from voima:
/lustre/tmp/uotilap/CFSR/MelbUni/0.5deg/G/exp01/MuniCFSRCyclones_all_exp001_????-????.nomet.sqlite
For example,
/lustre/tmp/uotilap/CFSR/MelbUni/0.5deg/G/exp01/MuniCFSRCyclones_all_exp001_2001-2001.nomet.sqlite

3. Calculate cyclone budget (stored in *.cpickle.gz files)
and generate plots:
$ python muni_erai_cyclone_budget_box_aa.py 2001 2001

4. Generate a supplementary plot:
$ python muni_erai_supplementary_plot_cyclone_budget_box_aa.py 2001 2001
