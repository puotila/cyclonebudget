#!/bin/env python
"""
Read annual cpickle files and plot cyclone fluxes.
Plot E and S transport rates as well as seasonal
track density differences. This is the supplementary figure.
(a) track density
(b) track density difference winter-summer
(c) cyclogenesis
(d) cyclolysis
(e) E transport
(f) S transport
"""

__Author__="<petteri.uotila@csiro.au>"
__Date__="21/08/2012"

import sys
import numpy
import cPickle, gzip
from muni_erai_cyclone_budget_box_aa import Domains, Domain

if __name__=="__main__":
        syear      = int(sys.argv[1])
        eyear      = int(sys.argv[2])
        years      = range(syear,eyear+1)
        months     = range(1,13)
        wmonths     = range(6,9) # winter months
        smonths     = range(1,4) # summer months
        basename   = "muni_erai_cyclone_budget_box_aa"
        dadir      = "./"
        # init domains
        domains = Domains(years)
        names = ['total','system','genesis','lysis',\
                 'outE','inW','outW','inE',\
                 'outS','inN','outN','inS']
        nyears = eyear-syear+1
        values = {}; wvalues = {}; svalues = {}
        for year in years:
            cfile = "%s/%s_%04d-%04d.domains.cpickle.gz" % (dadir,basename,year,year)
            fp = gzip.open(cfile)
            domains = cPickle.load(fp)
            fp.close()
            for name in names:
                for yidx,year in enumerate(domains.years):
                    for midx in range(12):
                        if midx+1 not in months: continue
                        if values.has_key(name):
                            values[name] += domains.get_values(name=name,midx=midx,yidx=yidx)
                        else:
                            values[name] = domains.get_values(name=name,midx=midx,yidx=yidx)
                        if midx+1 in wmonths:
                            if wvalues.has_key(name):
                                wvalues[name] += domains.get_values(name=name,midx=midx,yidx=yidx)
                            else:
                                wvalues[name] = domains.get_values(name=name,midx=midx,yidx=yidx)
                        if midx+1 in smonths:
                            if svalues.has_key(name):
                                svalues[name] += domains.get_values(name=name,midx=midx,yidx=yidx)
                            else:
                                svalues[name] = domains.get_values(name=name,midx=midx,yidx=yidx)
            print "Processed %04d" % year
            sys.stdout.flush()
        # values[name] = numpy.ma.mask(values[name],mask=values[name]==0)
        # plotting
        labels = ['(a)','(b)','(c)','(d)','(e)','(f)']
        clabs = ['$^\circ$lat$^{-2}$year$^{-1}$','$^\circ$lat$^{-2}$year$^{-1}$',\
                 '$^\circ$lat$^{-2}$year$^{-1}$','$^\circ$lat$^{-2}$year$^{-1}$',\
                 '$^\circ$lat$^{-1}$year$^{-1}$','$^\circ$lat$^{-1}$year$^{-1}$',\
                ]
        import matplotlib.pyplot as plt
        from mpl_toolkits import basemap
        m = basemap.Basemap(projection='spstere',boundinglat=-30.,lon_0=180.)
        domains.lon2d[numpy.where(domains.lon2d>180)] -= 360
        x, y = m(domains.lonb2d,domains.latb2d)
        dy = numpy.diff(domains.latb2d,axis=0)[:,:-1]
        dx = numpy.diff(domains.lonb2d,axis=1)[:-1,:]*numpy.cos(domains.lat2d*numpy.pi/180)
        fig = plt.figure(figsize=(11,6))
        for fidx,name in enumerate(['system','seadiff','genesis','lysis','outE','outS']):
            ax = fig.add_subplot(2,3,fidx+1)
            if name in ['seadiff']:
                img = m.pcolor(x,y,(wvalues['system']-svalues['system'])/(nyears*domains.areas),\
                         cmap='RdBu_r')
                clim = numpy.abs(img.get_clim()).max()
                img.set_clim(-clim,clim)
            else:
                img = m.pcolor(x,y,values[name]/(nyears*domains.areas),cmap='Reds')
            cb = fig.colorbar(img)
            cb.set_label(clabs[fidx])
            ax.set_title(labels[fidx])
            m.drawcoastlines()
            if fidx:
                m.drawmeridians([0,45,90,135,180,225,270,315],labels=[0,0,0,0],fontsize=12)
            else:
                m.drawmeridians([0,45,90,135,180,225,270,315],labels=[1,0,0,1],fontsize=12)
            m.drawparallels([-40,-50,-60,-70,-80],labels=[0,0,0,0],fontsize=12)
        for fsuf in ['png']:
            fname      = "%s_%04d-%04d_%02d-%02d_supp.%s" % \
                         (basename,years[0],years[-1],months[0],months[-1],fsuf)
            plt.savefig(fname)
        print "Finished!"
