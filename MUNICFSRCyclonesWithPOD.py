#!/bin/env python
"""
Inherits from MUNICyclonesWithPOD
which has CFSR specific netcdf meteorological reading routines
"""

__Author__  = 'P. Uotila, CSIRO <Petteri.Uotila@csiro.au>'
__Date__    = '05/07/10'
__Version__ = 0.1

import sys,os,numpy
sys.path.append(os.path.join(os.environ['HOME'],'lib','python2.7','site-pacages'))
import MUNICyclonesWithPOD

class CycloneLoc(MUNICyclonesWithPOD.CycloneLoc):
    """
    Override necessary routines in add_meteorological_vars():
    - read_meteorological_vars()
    - interpolate_variable()
    - get_horizontal_resolution()
    - read_grb_var() with varname mapping
    """

    # class wide declarations
    _Lat = {}; _Lon = {}; _Idxs = {}; _Dx = {}; _Dy = {}

    def __init__(self,line,genesis=False,lysis=False,years=[2000],**kwargs):
        MUNICyclonesWithPOD.CycloneLoc.__init__(self,line,genesis=genesis,\
                            lysis=lysis,years=years,**kwargs)
        self.metvarnames = ['shtfl']
        self.metvar_long_name = {}
        self.metvar_units = {}

    def read_meteorological_vars(self,ncname=None):
        """
        No need for ncname, we'll read from grib files directly,
        but kept for compatibility, since we override the original
        method.
        For online variables which are downloaded from mars server
        if required.
        """
        import netCDF4 as nc
        vals = {}; mval = {}
        for varname in self.metvarnames:
            if self.date.month==1 and self.date.day==1 and \
               self.date.hour==0 and self.date.year>1979:
                ncfn = "%s_%04d.cfsr.nc" % (varname,self.year-1)
            else:
                ncfn = "%s_%04d.cfsr.nc" % (varname,self.year)
            # read data from netcdf file
            fp = nc.Dataset(ncfn)
            self.lon  = numpy.array(fp.variables['lon'][:])
            self.lat  = numpy.array(fp.variables['lat'][:])
            time = fp.variables['time']
            cdftime = nc.netcdftime.utime(time.units,calendar=time.calendar)
            otime = cdftime.date2num(self.date)
            if self.date.month==1 and self.date.day==1 and \
               self.date.hour==0 and self.date.year==1979:
                tidx = 0 # first record in netcdf files is 1/1/06
                # here we use the first record of 1979 file
            else:
                tidx = numpy.where(numpy.array(time[:])==otime)[0][0]
            ncvar = fp.variables[varname]
            self.metvar_long_name[varname] = ncvar.long_name
            vals[varname] = numpy.ma.array(ncvar[tidx])
            if hasattr(ncvar,'missing_value'):
                mval[varname] = ncvar.missing_value
            elif hasattr(ncvar,'_FillValue'):
                mval[varname] = ncvar._FillValue
            else:
                mval[varname] = -999.999
            if hasattr(ncvar,'units'):
                self.metvar_units[varname] = ncvar.units
            else:
                self.metvar_units[varname] = ""
            fp.close()
        return vals, mval

    def __read_meteorological_vars_old(self,ncname=None):
        """
        No need for ncname, we'll read from grib files directly,
        but kept for compatibility, since we override the original
        method. Not used anymore. For IL predownloaded files in 2010.
        """
        vars = {}; mval = {}
        self.get_lat_lon_grids()
        # could extend varnames since erai output contains more vars than amps
        for varname in ['terrain','sst','u','v','rain_non','rain_con','shflux','lhflux',\
                        'lwdown','swdown','ground_t','sw','nsss','ewss','lw']:
            vars[varname] = self.read_grbvar(varname)
            mval[varname] = -999.999
        sst = numpy.copy(vars['sst'])
        sst[numpy.where(vars['sst']<271.)]=vars['ground_t'][numpy.where(vars['sst']<271.)]
        vars['sst'] = sst
        vars['ust'] = numpy.sqrt(numpy.sqrt(numpy.power(vars['nsss'],2)+numpy.power(vars['ewss'],2))/1.25)
        mval['ust'] = mval['vor'] = mval['div'] = -999.999
        # assume Arakawa A i.e. all vars in same points -> 2nd degree scheme
        # ADD comp 2*ds
        dx,dy = self.get_horizontal_resolution() # km
        dudx = numpy.zeros(vars['u'].shape) # 10**3 1/s
        dudx[1:-1,:] = (vars['u'][2:,:]-vars['u'][:-2,:])/dx[1:-1,:]
        dudy = numpy.zeros(vars['u'].shape) # 10**3 1/s
        dudy[:,1:-1] = (vars['u'][:,2:]-vars['u'][:,:-2])/dy[:,1:-1]
        dvdx = numpy.zeros(vars['u'].shape) # 10**3 1/s
        dvdx[1:-1,:] = (vars['v'][2:,:]-vars['v'][:-2,:])/dx[1:-1,:]
        dvdy = numpy.zeros(vars['u'].shape) # 10**3 1/s
        dvdy[:,1:-1] = (vars['v'][:,2:]-vars['v'][:,:-2])/dy[:,1:-1]
        vars['div'] = dudx + dvdy
        vars['vor'] = dvdx - dudy
        return vars, mval

    def interpolate_variable(self,var,func=numpy.average):
        """
        idxs.weights contain indices required for computations
        """
        from common_functions import haversine
        lon2d, lat2d = numpy.meshgrid(self.lon,self.lat)
        R = 6372795.
        dst = R*haversine(self.x,lon2d,self.y,lat2d)
        idxs = numpy.where(dst<self.rd*111500.)
        try:
            return func(var[idxs])
        except:
            print "WARNING: interpolation failed!"
            return None

    def add_cice_vars(self):
        """ -999.0 over land otherwise [0...1]
        """
        varname = 'cice'
        var_in = numpy.copy(self.read_grbvar(varname))
        tsk = self.read_grbvar('ground_t')
        grbname, lname, vtype = self.map_grbvar(varname)
        var_in[numpy.where((var_in<0.) & (tsk>=271.35))]=0.
        var_in[numpy.where((var_in<0.) & (tsk<271.35))]=1.
        var_out = (self.interpolate_variable(var_in,vtype=vtype),\
                           self.interpolate_variable(var_in,func=numpy.var,vtype=vtype),\
                           self.interpolate_variable(var_in,func=numpy.max,vtype=vtype),\
                           self.interpolate_variable(var_in,func=numpy.min,vtype=vtype))        
        setattr(self,varname,var_out)
        CycloneLoc._Idxs = {}

    def add_meteorological_vars(self):
        vars_in, mval = self.read_meteorological_vars()
        for varname in self.metvarnames:
            grbname, lname, vtype = self.map_grbvar(varname)
            if varname in ['ds_sst','ds_ground_t']:
                var_in_name = varname.split('_',1)[-1]
                missing_value=mval[var_in_name]
                var_in = self.get_ds_temp(vars_in[var_in_name],missing_value=missing_value)
            elif varname=='ws':
                missing_value=mval['u']
                u = vars_in['u']; v = vars_in['v']
                var_in = numpy.sqrt(u**2 + v**2)
            elif varname=='rain':
                missing_value=mval['rain_non']
                var_in = vars_in['rain_non']+vars_in['rain_con']
            elif varname=='albedo':
                missing_value=mval['sw']
                # swdown is total sw radiation
                # sw = (1-albedo)*swdown
                # both are positive
                var_in = (vars_in['swdown']-vars_in['sw'])/vars_in['swdown']
                var_in[numpy.where(var_in<0.)] = 0.5 # a wild guess
            elif varname=='swup':
                missing_value=mval['sw']
                var_in = vars_in['sw']-vars_in['swdown'] # negative up
            elif varname=='lwup':
                # erai lwdown is always positive (down), but lw can be both
                missing_value=mval['lw']
                var_in = vars_in['lw']-vars_in['lwdown'] # negative up
            elif varname=='te':
                missing_value=mval['swdown']
                var_in = vars_in['lw']+vars_in['shflux']+\
                    +vars_in['lhflux']+vars_in['sw'] # negative up
                # positive turbulent fluxes are downwards
                # in erai shflux and lhflux are mainly negative, i.e. surface loses energy to atmosphere,
                # which makes sense
            else:
                missing_value=mval[varname]
                var_in = vars_in[varname]
            # turn erai fluxes positive up (as in AMPS)
            sgn = -1. if varname in ['lw','lwdown','lwup','sw','swdown','swup','te','shflux','lhflux'] else 1.
            if varname in ['terrain']: sgn = 1./9.81
            var_out = (self.interpolate_variable(sgn*var_in),\
                           self.interpolate_variable(sgn*var_in,func=numpy.var),\
                           self.interpolate_variable(sgn*var_in,func=numpy.max),\
                           self.interpolate_variable(sgn*var_in,func=numpy.min))
            setattr(self,varname,var_out)
        return 0

    def get_ds_temp(self,var,missing_value=-999.999):
        dx,dy = self.get_horizontal_resolution() # km
        dx_var = numpy.zeros(var.shape,dtype='float')
        dx_var[1:-1,:] = (var[2:,:]-var[:-2,:])/dx[1:-1,:] # 10**3 K/m
        dy_var = numpy.zeros(var.shape,dtype='float')
        dy_var[:,1:-1] = (var[:,2:]-var[:,:-2])/dy[:,1:-1]
        return numpy.sqrt(dx_var**2 + dy_var**2)        

    def get_lat_lon_grids(self):
        for vtype in ['an','fc','te']:
            if not CycloneLoc._Lon.has_key(vtype):
                lon1d = self.read_grbvar('lon'+vtype)
                lat1d = self.read_grbvar('lat'+vtype)
                lon2d,lat2d = numpy.meshgrid(lon1d,lat1d)
                CycloneLoc._Lon[vtype] = lon2d; CycloneLoc._Lat[vtype] = lat2d
                if vtype=='an':
                    CycloneLoc._Lon['P'] = lon2d; CycloneLoc._Lat['P'] = lat2d
        return 0

    def get_horizontal_resolution(self,vtype='an'):
        """ horizontal resolution in km
        dx and dy
        """
        if CycloneLoc._Dx.has_key(vtype):
            dx = CycloneLoc._Dx[vtype]
            dy = CycloneLoc._Dy[vtype]
        else:
            dx = numpy.ones(CycloneLoc._Lon[vtype].shape)
            dy = numpy.ones(CycloneLoc._Lat[vtype].shape)
            lon2d = CycloneLoc._Lon[vtype]
            lat2d = CycloneLoc._Lat[vtype]
            dy[1:-1,:] = numpy.abs(lat2d[2:,:]-lat2d[:-2,:])*111.5 # km
            dx[:,1:-1] = numpy.abs((lon2d[:,2:]-lon2d[:,:-2])*numpy.cos(lat2d[:,1:-1]*numpy.pi/180.0))*111.5 # km
            CycloneLoc._Dx[vtype] = dx
            CycloneLoc._Dy[vtype] = dy
        return dx,dy

    def form_lowres_grbname(self,date,path='/nfs/5/home/rclimateuser/puotila/erai/vars-for-john-pearce'):
        import re,glob
        from datetime import datetime, timedelta
        grbnames = glob.glob(os.path.join(path,'ei_oper_fc_sfc_15x15_90N0E90S3585E_????????_????????.grb'))
        for grbname in grbnames:
            m = re.match(".+_(\d{8})_(\d{8}).grb",grbname)
            if m:
                dstart = datetime(int(m.group(1)[:4]),int(m.group(1)[4:6]),int(m.group(1)[6:]))
                dend   = datetime(int(m.group(2)[:4]),int(m.group(2)[4:6]),int(m.group(2)[6:])) + \
                         timedelta(days=1)
                if dstart <= date < dend:
                    return grbname
        return None

    def read_grbvar(self,varname,tmpdir = os.path.join('/scratch',os.getenv('USER'))):
        """
        Search and extract grib file containing varname on date
        """
        import tarfile,shutil,Nio
        from datetime import datetime, timedelta
        date = self.date
        grbname, lname, vtype = self.map_grbvar(varname)
        if vtype in ['te']:
            pth = os.path.join(os.getenv('HOME'),'puotila','data','CFSRhires')
            grbfn = 'ERA_Interim_orography'
            if not os.path.exists(grbfn+'.grb'):
                os.symlink(os.path.join(pth,grbfn+'.grib'),grbfn+'.grb')
        else:
            pref = 'Pet_'
            if vtype=='P':
                if date.hour==0:
                    ihour="0"; fhour=24.
                elif date.hour==12:
                    ihour="1200"; fhour=24.
                elif date.hour==18:
                    ihour="0"; fhour=18.
                    pref = 'Pet_18'
                else: # 6utc
                    ihour="1200"; fhour=18.
                    pref = 'Pet_18'
            elif vtype=='fc':
                if date.hour==0:
                    ihour="1200"; fhour=12.
                    pref = 'Pet_12'
                elif date.hour==12:
                    ihour="0"; fhour=12.
                    pref = 'Pet_12'
                elif date.hour==18:
                    ihour="1200"; fhour=6.
                else: # 6utc
                    ihour="0"; fhour=6.
            else: # an
                fhour = 0.
                ihour = "%d00" % date.hour if date.hour else "0"
            fdate = date-timedelta(days=fhour/24)
            if fdate<datetime(1989,1,1):
                fdate=datetime(1989,1,1)
                if vtype=='fc':
                    ihour="0"; fhour=6.; pref= 'Pet_' # 1st avail fc
                if vtype=='P':
                    ihour="0"; fhour=12.; pref= 'Pet_12' # 1st avail P
            if fdate>=datetime(2009,12,31,18): fdate=datetime(2009,12,31,18)
            pth = os.path.join(os.getenv('HOME'),'puotila','data','CFSRhires',"%04d" % fdate.year)
            tarfn = os.path.join(pth,'%s%s%04d%02d.tar' % (pref,vtype,fdate.year,fdate.month))
            grbfn = os.path.join(tmpdir,'%s%s%04d%02d%02d+%s+%d.sfc' % \
                                 (pref,vtype,fdate.year,fdate.month,fdate.day,ihour,int(fhour)))
            if not os.path.exists(grbfn+'.grb'):
                tar = tarfile.open(tarfn,'r')
                tar.extract(os.path.basename(grbfn),path=tmpdir)
                tar.close()
                os.rename(grbfn,grbfn+'.grb')
            #print "%s: %s" % (date,grbfn)
        try:
                f = Nio.open_file(grbfn+'.grb','r')
                grbvar = f.variables[grbname][:]
                f.close()
        except:
                print "Reading %s failed!" % grbfn
                sys.exit(1)
        return grbvar

    def map_grbvar(self,varname):
        """
        Maps cyclones db varnames with erai grb varnames.
        table[varname] = [grbname,long_name,unit?]
        """
        table = {\
            'terrain':['Z_GDS0_SFC','Geopotential height','te'],\
            'latte':['g0_lat_0','latitude','te'],\
            'lonte':['g0_lon_1','longitude','te'],\
            'latan':['g0_lat_0','latitude','an'],\
            'lonan':['g0_lon_1','longitude','an'],\
            'latfc':['g0_lat_0','latitude','fc'],\
            'lonfc':['g0_lon_1','longitude','fc'],\
            'latP':['g0_lat_0','latitude','P'],\
            'lonP':['g0_lon_1','longitude','P'],\
            'lhflux':['SLHF_GDS0_SFC','Surface latent heat flux','fc'],\
            'sw':['SSR_GDS0_SFC','Surface solar radiation','fc'],\
            'nsss':['NSSS_GDS0_SFC','North-South surface stress','fc'],\
            'lwdown':['STRD_GDS0_SFC','Surface thermal radiation downwards','fc'],\
            'shflux':['SSHF_GDS0_SFC','Surface sensible heat flux','fc'],\
            'shtfl':['shtfl','Sensible heat net flux','fc'],\
            'blh':['BLH_GDS0_SFC','Boundary layer height','fc'],\
            'lw':['STR_GDS0_SFC','Surface thermal radiation','fc'],\
            'swdown':['SSRD_GDS0_SFC','Surface solar radiation downwards','fc'],\
            'ewss':['EWSS_GDS0_SFC','East-West surface stress','fc'],\
            'sp':['SP_GDS0_SFC','Surface pressure','an'],\
            'tcc':['TCC_GDS0_SFC','Total cloud cover','an'],\
            'lsm':['LSM_GDS0_SFC','Land-sea mask','an'],\
            'tcwv':['TCWV_GDS0_SFC','Total column water vapour','an'],\
            'tcw':['TCW_GDS0_SFC','Total column water','an'],\
            'tdew2m':['2D_GDS0_SFC','2 metre dewpoint temperature','an'],\
            'msl':['MSL_GDS0_SFC','Mean sea level pressure','an'],\
            'hcc':['HCC_GDS0_SFC','High cloud cover','an'],\
            'u':['10U_GDS0_SFC','10 metre U wind component','an'],\
            'mcc':['MCC_GDS0_SFC','Medium cloud cover','an'],\
            't2m':['2T_GDS0_SFC','2 metre temperature','an'],\
            'sst':['SSTK_GDS0_SFC','Sea surface temperature','an'],\
            'cice':['CI_GDS0_SFC','Sea-ice cover','an'],\
            'lcc':['LCC_GDS0_SFC','Low cloud cover','an'],\
            'ground_t':['SKT_GDS0_SFC','Skin temperature','an'],\
            'v':['10V_GDS0_SFC','10 metre V wind component','an'],\
            'rain_con':['CP_GDS0_SFC','Convective precipitation','P'],\
            'snow':['SF_GDS0_SFC','Snowfall (convective + stratiform)','P'],\
            'rain_non':['LSP_GDS0_SFC','Stratiform precipitation (Large scale precipitation)','P'],\
            # additional vars (not in default set)
            'ishf':['ISHF_GDS0_SFC','Instantaneous surface heat flux','fc'],\
            'inss':['INSS_GDS0_SFC','Instantaneous Y surface stress','fc'],\
            'lwgs':['LGWS_GDS0_SFC','Latitudinal component of gravity wave stress','fc'],\
            'tdew2m':['2D_GDS0_SFC','2 metre dewpoint temperature','fc'],\
            'ttrc':['TTRC_GDS0_SFC','Top net thermal radiation, clear sky','fc'],\
            'tsr':['TSR_GDS0_SFC','Top solar radiation','fc'],\
            'gust':['10FG_GDS0_SFC','10 metre wind gust','fc'],\
            'si':['SI_GDS0_SFC','Solar insolation','fc'],\
            'bld':['BLD_GDS0_SFC','Boundary layer dissipation','fc'],\
            'mgws':['MGWS_GDS0_SFC','Meridional component of gravity wave stress','fc'],\
            'strc':['STRC_GDS0_SFC','Surface net thermal radiation, clear sky','fc'],\
            'albedo':['FAL_GDS0_SFC','Forecast albedo','fc'],\
            'ssrc':['SSRC_GDS0_SFC','Surface net solar radiation, clear sky','fc'],\
            'e':['E_GDS0_SFC','Evaporation','fc'],\
            'lspf':['LSPF_GDS0_SFC','Large-scale precipitation fraction','fc'],\
            'gwd':['GWD_GDS0_SFC','Gravity wave dissipation','fc'],\
            'csf':['CSF_GDS0_SFC','Convective snowfall','fc'],\
            'forecast_time1':['forecast_time1','Forecast offset from initial time','fc'],\
            'uvb':['UVB_GDS0_SFC','Downward UV radiation at the surface','fc'],\
            'cape':['CAPE_GDS0_SFC','Convective available potential energy','fc'],\
            'initial_time0_encoded':['initial_time0_encoded','initial time encoded as double','fc'],\
            'lsf':['LSF_GDS0_SFC','Large-scale snowfall','fc'],\
            'sund':['SUND_GDS0_SFC','Sunshine duration','fc'],\
            'initial_time0':['initial_time0','Initial time of first record','fc'],\
            'es':['ES_GDS0_SFC','Snow evaporation','fc'],\
            'initial_time0_hours': ['initial_time0_hours','initial time','fc'],\
            # conducted vars (computed from grbvars)
            'ws':['ws','10 metre wind','an'],\
            'vor':['vor','vorticity','an'],\
            'div':['div','divergence','an'],\
            'ust':['ust','friction velocity','fc'],\
            'ds_sst':['ds_sst','SST gradient','an'],\
            'ds_ground_t':['ds_ground_t','Tskin gradient','an'],\
            'swup':['swup','Surface solar radiation up','fc'],\
            'lwup':['lwup','Surface thermal radiation up','fc'],\
            'rain':['rain','Total Precipitation','P'],\
            'te':['te','Surface net energy','fc'],\
            }
        if table.has_key(varname):
            return table[varname]
        else:
            print "WARNING: Cant map %s!" % varname
            return [varname,'unknown','an']

    def CloseBoundary(self):
        """
        For global data we are always away from boundaries
        """
        return False

class Cyclone(MUNICyclonesWithPOD.Cyclone):
    def initCycloneLoc(self,locationline,genesis,lysis):
        return CycloneLoc(locationline,genesis=genesis,lysis=lysis,years=self.years)

class Cyclones(MUNICyclonesWithPOD.Cyclones):
    def initCyclone(self,lines,li,year):
        return Cyclone(lines[li],years=[year-1,year,year+1])

